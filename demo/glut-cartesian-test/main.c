#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <libreplot/libreplot.h>

#define SAMPLES_COUNT (96*1000*3)

lp_cartesian *cartesian = NULL;
lp_cartesian_axis *cartesian_axis_bottom = NULL;
lp_cartesian_axis *cartesian_axis_left = NULL;
lp_line *line = NULL;
lp_circle *circle = NULL;
lp_text *text = NULL;
lp_cartesian_curve *curve = NULL;
lp_rectangle *rect = NULL;

lp_vec2f samples[SAMPLES_COUNT];

void init_samples()
{
	/* Some data */
	for (int i = 0; i < SAMPLES_COUNT; i++) {
		float x = (i - (SAMPLES_COUNT / 2.0)) / 100.0;
		samples[i].x = x;
		if (fabs(x) < 1) {
			samples[i].y = 0;
		} else if (fabs(x) < 2) {
			samples[i].y = 1;
		} else if (fabs(x) < 3) {
			samples[i].y = -1;
		} else {
			samples[i].y = sin(x * 10.0) / (x * x * 0.5);
		}
	}

	curve = lp_cartesian_curve_gen();

	/* x data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_X, 0,
		GL_FLOAT, sizeof(lp_vec2f), &samples[0].x, SAMPLES_COUNT);

	/* y data */
	lp_cartesian_curve_data(curve, LP_CARTESIAN_CURVE_Y, 0,
		GL_FLOAT, sizeof(lp_vec2f), &samples[0].y, SAMPLES_COUNT);
}

int init_resources() {

	lp_init();

	cartesian = lp_cartesian_gen();

	lp_cartesian_2float(cartesian, LP_CARTESIAN_DPI, 100, 100);

	cartesian_axis_bottom = lp_cartesian_axis_gen();
	//~ lp_cartesian_axis_4float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_BASELINE_COLOR, 0.5, 0, 0, 1);
	//~ lp_cartesian_axis_float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_BASELINE_WIDTH, 5);
	lp_cartesian_axis_bool(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_BASELINE_SHOW, true);
	lp_cartesian_axis_pointer(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_TEXT, "Bottom");
	lp_cartesian_axis_float(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_HEIGHT, 20);
	lp_cartesian_axis_bool(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_SHOW, true);
	//~ lp_cartesian_axis_float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_TICK_WIDTH, 5);
	lp_cartesian_axis_pointer(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_APPEND, "V");
	lp_cartesian_axis_enum(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);
	lp_cartesian_axis_2float(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_RANGE, -15, 15);

	cartesian_axis_left = lp_cartesian_axis_gen();
	//~ lp_cartesian_axis_4float(cartesian_axis_left,
		//~ LP_CARTESIAN_AXIS_BASELINE_COLOR, 1, 0, 0, 1);
	//~ lp_cartesian_axis_float(cartesian_axis_left,
		//~ LP_CARTESIAN_AXIS_BASELINE_WIDTH, 5);
	lp_cartesian_axis_bool(cartesian_axis_left,
		LP_CARTESIAN_AXIS_BASELINE_SHOW, true);
	lp_cartesian_axis_pointer(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_TEXT, "Left");
	lp_cartesian_axis_float(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_HEIGHT, 15);
	lp_cartesian_axis_bool(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_SHOW, true);
	lp_cartesian_axis_2float(cartesian_axis_left,
		LP_CARTESIAN_AXIS_VALUE_RANGE, -4, 3);

	lp_cartesian_pointer(cartesian,
		LP_CARTESIAN_AXIS_AT_LEFT, cartesian_axis_left);

	lp_cartesian_pointer(cartesian,
		LP_CARTESIAN_AXIS_AT_BOTTOM, cartesian_axis_bottom);

	line = lp_line_gen();
	lp_line_2float(line, LP_LINE_START, 0, 0);
	lp_line_2float(line, LP_LINE_END, +INFINITY, +INFINITY);

	circle = lp_circle_gen();
	lp_circle_float(circle, LP_CIRCLE_RADIUS, 0.5);
	lp_circle_2float(circle, LP_CIRCLE_CENTER, 0, 0);
	lp_circle_bool(circle, LP_CIRCLE_FILL_SHOW, true);
	lp_circle_bool(circle, LP_CIRCLE_LINE_SHOW, true);
	lp_circle_float(circle, LP_CIRCLE_LINE_WIDTH, 10);
	lp_circle_4float(circle, LP_CIRCLE_FILL_COLOR, 1, 0, 0, 1);

	text = lp_text_gen();
	lp_text_pointer(text, LP_TEXT_STRING, "Hello world!");
	lp_text_2float(text, LP_TEXT_POSITION, -.5, .7);
	lp_text_float(text, LP_TEXT_HEIGHT, 40);
	lp_text_4float(text, LP_TEXT_COLOR, 1, 0, 0, 1);

	rect = lp_rectangle_gen();
	lp_rectangle_4float(rect, LP_RECTANGLE_GEOMETRY, 0, 0, 3, 3);
	lp_rectangle_bool(rect, LP_RECTANGLE_FILL_SHOW, true);
	lp_rectangle_bool(rect, LP_RECTANGLE_LINE_SHOW, true);
	lp_rectangle_float(rect, LP_RECTANGLE_LINE_WIDTH, 10);
	lp_rectangle_4float(rect, LP_RECTANGLE_FILL_COLOR, 1, 0, 0, 1);

	init_samples();

	return 1;
}

void window_resize(int width, int height)
{
	//width = height = width < height ? width : height;

	glViewport(0, 0, width, height);
	lp_cartesian_4uint(cartesian, LP_CARTESIAN_SURFACE, 0, 0, width, height);
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);

	lp_cartesian_draw_start(cartesian);
	//lp_cartesian_draw_line(cartesian, line);
	//lp_cartesian_draw_circle(cartesian, circle);
	//lp_cartesian_draw_text(cartesian, text);
	lp_cartesian_draw_rectangle(cartesian, rect);

	lp_cartesian_draw_curve(cartesian, curve);

	lp_cartesian_draw_end(cartesian);

	glutSwapBuffers();
}

void free_resources() {
	lp_cartesian_del(cartesian);
	lp_line_del(line);
	lp_circle_del(circle);
	lp_text_del(text);
	lp_cartesian_curve_del(curve);
	lp_rectangle_del(rect);
}

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutCreateWindow("libreplot");

	lp_enum glew_status = glewInit();

	if (GLEW_OK != glew_status) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "No support for OpenGL 2.0 found\n");
		return 1;
	}

	if (init_resources()) {
		glutReshapeFunc(window_resize);
		glutDisplayFunc(display);
		glutMainLoop();
	}

	free_resources();
	return 0;
}
