# Make sure the compiler can find include files from our Hello library.
#include_directories (${HELLO_SOURCE_DIR}/Hello)

# Make sure the linker can find the Hello library once it is built.
LINK_DIRECTORIES (${BOX0_BINARY_DIR}/libreplot)

INCLUDE_DIRECTORIES(${PROJECT_BINARY_DIR}/libreplot ${PROJECT_SOURCE_DIR}/libreplot)

CMAKE_POLICY(SET CMP0020 NEW)

FIND_PACKAGE(Qt5Widgets REQUIRED)
INCLUDE_DIRECTORIES(${Qt5Widgets_INCLUDE_DIRS})
ADD_DEFINITIONS(${Qt5Widgets_DEFINITIONS})

FIND_PACKAGE(Qt5PrintSupport REQUIRED)
INCLUDE_DIRECTORIES(${Qt5PrintSupport_INCLUDE_DIRS})
ADD_DEFINITIONS(${Qt5PrintSupport_DEFINITIONS})

FIND_PACKAGE(Qt5OpenGL REQUIRED)
INCLUDE_DIRECTORIES(${Qt5OpenGL_INCLUDE_DIRS})
ADD_DEFINITIONS(${Qt5OpenGL_DEFINITIONS})

# Qt5 require PIC
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# Add executable called "helloDemo" that is built from the source files
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR})
ADD_EXECUTABLE(qt-cartesian-demo main.cpp cartesian.cpp)

# Link the executable to the Hello library.
TARGET_LINK_LIBRARIES(qt-cartesian-demo replot)
TARGET_LINK_LIBRARIES(qt-cartesian-demo ${Qt5Widgets_LIBRARIES})
TARGET_LINK_LIBRARIES(qt-cartesian-demo ${Qt5PrintSupport_LIBRARIES})
TARGET_LINK_LIBRARIES(qt-cartesian-demo ${Qt5OpenGL_LIBRARIES})
