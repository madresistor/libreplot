/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cartesian.h"
#include <QMouseEvent>
#include <QApplication>
#include <QDesktopWidget>
#include <QPaintDevice>
#include <Qt>
#include <math.h>

Cartesian::Cartesian(QWidget *parent):
	QOpenGLWidget(parent),
	m_cartesian(NULL),
	m_axis_left(NULL),
	m_axis_bottom(NULL),
	m_curve(NULL),
	m_circle(NULL),
	m_text(NULL),
	m_surface_width(0),
	m_surface_height(0)
{
	m_data_len = 300000;
	m_data = (lp_vec2f *) malloc(sizeof(lp_vec2f) * m_data_len);

	for (uint i = 0; i < m_data_len; i++) {
		float x = (i -(m_data_len / 2.0)) / 10.;
		float y = sin(x);
		m_data[i].x = x;
		m_data[i].y = y;
	}
}

Cartesian::~Cartesian()
{
	makeCurrent();
	lp_cartesian_del(m_cartesian);
	lp_cartesian_axis_del(m_axis_left);
	lp_cartesian_axis_del(m_axis_bottom);
	lp_cartesian_curve_del(m_curve);
	lp_circle_del(m_circle);
	lp_text_del(m_text);
	free(m_data);
	doneCurrent();
}

void Cartesian::mousePressEvent(QMouseEvent *e)
{
	if (e->buttons() & (Qt::LeftButton | Qt::RightButton)) {
		last_x = e->x();
		last_y = e->y();
		e->accept();
	}
}

static void axis_translate(lp_cartesian_axis *axis, int surface, float diff)
{
	float min, max, tmp;
	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = (max - min) * (diff  / surface);
	min -= tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

static void axis_scale(lp_cartesian_axis *axis, int surface, float diff)
{
	float min, max, tmp;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = ((max - min) / 2) * (diff / surface);
	tmp *= 5; /* speed up */
	min += tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::mouseMoveEvent(QMouseEvent *e)
{
	int x = e->x(), y = e->y();

	/* -1 multiplied because:
	 *  from top->bottom [mouse coordinate]
	 *  to bottom->top [graph].
	 * (only for Y axis) */

	if (e->buttons() & Qt::LeftButton) {
		axis_translate(m_axis_left, m_surface_height, -1 * (y - last_y));
		axis_translate(m_axis_bottom, m_surface_width, (x - last_x));
	} else if (e->buttons() & Qt::RightButton) {
		axis_scale(m_axis_left, m_surface_height, -1 * (y - last_y));
		axis_scale(m_axis_bottom, m_surface_width, x - last_x);
	} else {
		return;
	}

	last_x = x;
	last_y = y;

	e->accept();
	update();
}

void Cartesian::mouseReleaseEvent(QMouseEvent *e)
{
	e->accept();
	update();
}

static void axis_scroll(lp_cartesian_axis *axis, int steps)
{
	float min, max, tmp;

	lp_get_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, &min, &max);

	tmp = ((max - min) / 6) * steps;
	min += tmp;
	max -= tmp;

	lp_cartesian_axis_2float(axis, LP_CARTESIAN_AXIS_VALUE_RANGE, min, max);
}

void Cartesian::wheelEvent(QWheelEvent *e)
{
	QPoint numPixels = e->pixelDelta();
	QPoint numDegrees = e->angleDelta() / 8;
	qreal numSteps;

	if (!numPixels.isNull()) {
		numSteps = numPixels.y() / 15.0;
	} else if (!numDegrees.isNull()) {
		numSteps = numDegrees.y() / 15.0;
	} else {
		numSteps = 0;
	}

	axis_scroll(m_axis_left, numSteps);
	axis_scroll(m_axis_bottom, numSteps);
	update();

	e->accept();
}

void Cartesian::initializeGL()
{
	initializeOpenGLFunctions();

	lp_init();

	m_cartesian = lp_cartesian_gen();
	m_dpi.x = physicalDpiX();
	m_dpi.y = physicalDpiY();
	lp_cartesian_2float(m_cartesian, LP_CARTESIAN_DPI, m_dpi.x, m_dpi.y);

	m_axis_bottom = lp_cartesian_axis_gen();
	m_axis_left = lp_cartesian_axis_gen();

	lp_cartesian_axis_2float(m_axis_bottom, LP_CARTESIAN_AXIS_VALUE_RANGE, -10, +10);
	lp_cartesian_axis_2float(m_axis_left, LP_CARTESIAN_AXIS_VALUE_RANGE, -10, +10);

	lp_cartesian_pointer(m_cartesian, LP_CARTESIAN_AXIS_AT_LEFT, m_axis_left);
	lp_cartesian_pointer(m_cartesian, LP_CARTESIAN_AXIS_AT_BOTTOM, m_axis_bottom);

	m_curve = lp_cartesian_curve_gen();

	lp_cartesian_curve_4float(m_curve, LP_CARTESIAN_CURVE_LINE_COLOR, 0, 0, 1, 1);
	lp_cartesian_curve_float(m_curve, LP_CARTESIAN_CURVE_LINE_WIDTH, 3);

	lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_X,
		0, GL_FLOAT, sizeof(lp_vec2f), &m_data[0].x, m_data_len);

	lp_cartesian_curve_data(m_curve, LP_CARTESIAN_CURVE_Y,
		0, GL_FLOAT, sizeof(lp_vec2f), &m_data[0].y, m_data_len);

	lp_texture *marker = lp_texture_gen_text((uint8_t *)"◩", 7, lp_font_gen(), &m_dpi);
	//lp_texture *marker = lp_texture_gen_png_file("/home/kuldeep/Desktop/not-gate-md.png");
	lp_cartesian_curve_pointer(m_curve, LP_CARTESIAN_CURVE_MARKER_TEXTURE, marker);
	lp_texture_del(marker);

	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_MARKER_SHOW, true);
	lp_cartesian_curve_bool(m_curve, LP_CARTESIAN_CURVE_LINE_SHOW, false);

	m_circle = lp_circle_gen();
	lp_circle_float(m_circle, LP_CIRCLE_RADIUS, 3);

	m_text = lp_text_gen();
	lp_text_pointer(m_text, LP_TEXT_STRING, (void *) "Libreplot");
}

void Cartesian::resizeGL(int w, int h)
{
	glViewport(0, 0, w, h);
	lp_cartesian_4uint(m_cartesian, LP_CARTESIAN_SURFACE, 0, 0, w, h);
	m_surface_width = w;
	m_surface_height = h;
}

void Cartesian::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT);
	lp_cartesian_draw_start(m_cartesian);
	lp_cartesian_draw_curve(m_cartesian, m_curve);
	lp_cartesian_draw_circle(m_cartesian, m_circle);
	lp_cartesian_draw_text(m_cartesian, m_text);
	lp_cartesian_draw_end(m_cartesian);
}
