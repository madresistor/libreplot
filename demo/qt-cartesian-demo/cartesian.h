/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CARTESIAN_H
#define CARTESIAN_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QQuaternion>
#include <QVector2D>
#include <libreplot/libreplot.h>

class Cartesian : public QOpenGLWidget, protected QOpenGLFunctions
{

public:
	Cartesian(QWidget *parent = 0);
	~Cartesian();

protected:
	void mousePressEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent * event) Q_DECL_OVERRIDE;

	void initializeGL() Q_DECL_OVERRIDE;
	void resizeGL(int w, int h) Q_DECL_OVERRIDE;
	void paintGL() Q_DECL_OVERRIDE;
	lp_cartesian *m_cartesian;
	lp_cartesian_axis *m_axis_left;
	lp_cartesian_axis *m_axis_bottom;
	lp_cartesian_curve *m_curve;
	lp_circle *m_circle;
	lp_text *m_text;
	lp_vec2f *m_data;
	lp_vec2f m_dpi;
	uint m_data_len;

	bool translate_active, scale_active;
	int last_x, last_y;

	int m_surface_width, m_surface_height;
};

#endif
