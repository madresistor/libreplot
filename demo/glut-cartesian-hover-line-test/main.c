#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <libreplot/libreplot.h>

lp_cartesian *cartesian = NULL;
lp_cartesian_axis *cartesian_axis_bottom = NULL;
lp_cartesian_axis *cartesian_axis_left = NULL;
lp_line *line = NULL;

bool show_line = false;
int area_height;

int init_resources() {

	lp_init();

	cartesian = lp_cartesian_gen();

	lp_cartesian_2float(cartesian, LP_CARTESIAN_DPI, 100, 100);

	cartesian_axis_bottom = lp_cartesian_axis_gen();
	//~ lp_cartesian_axis_4float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_BASELINE_COLOR, 0.5, 0, 0, 1);
	//~ lp_cartesian_axis_float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_BASELINE_WIDTH, 5);
	lp_cartesian_axis_bool(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_BASELINE_SHOW, true);
	lp_cartesian_axis_pointer(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_TEXT, "Bottom");
	lp_cartesian_axis_float(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_HEIGHT, 20);
	lp_cartesian_axis_bool(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_NAME_SHOW, true);
	//~ lp_cartesian_axis_float(cartesian_axis_bottom,
		//~ LP_CARTESIAN_AXIS_TICK_WIDTH, 5);
	lp_cartesian_axis_pointer(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_APPEND, "V");
	lp_cartesian_axis_enum(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_PREFIX_SELECT,
		LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC);
	lp_cartesian_axis_2float(cartesian_axis_bottom,
		LP_CARTESIAN_AXIS_VALUE_RANGE, -15, 15);

	cartesian_axis_left = lp_cartesian_axis_gen();
	//~ lp_cartesian_axis_4float(cartesian_axis_left,
		//~ LP_CARTESIAN_AXIS_BASELINE_COLOR, 1, 0, 0, 1);
	//~ lp_cartesian_axis_float(cartesian_axis_left,
		//~ LP_CARTESIAN_AXIS_BASELINE_WIDTH, 5);
	lp_cartesian_axis_bool(cartesian_axis_left,
		LP_CARTESIAN_AXIS_BASELINE_SHOW, true);
	lp_cartesian_axis_pointer(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_TEXT, "Left");
	lp_cartesian_axis_float(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_HEIGHT, 15);
	lp_cartesian_axis_bool(cartesian_axis_left,
		LP_CARTESIAN_AXIS_NAME_SHOW, true);
	lp_cartesian_axis_2float(cartesian_axis_left,
		LP_CARTESIAN_AXIS_VALUE_RANGE, -4, 3);

	lp_cartesian_pointer(cartesian,
		LP_CARTESIAN_AXIS_AT_LEFT, cartesian_axis_left);

	lp_cartesian_pointer(cartesian,
		LP_CARTESIAN_AXIS_AT_BOTTOM, cartesian_axis_bottom);

	line = lp_line_gen();
	lp_line_2float(line, LP_LINE_START, 0, -INFINITY);
	lp_line_2float(line, LP_LINE_END, 0, +INFINITY);

	return 1;
}

void window_resize(int width, int height)
{
	//width = height = width < height ? width : height;

	glViewport(0, 0, width, height);
	lp_cartesian_4uint(cartesian, LP_CARTESIAN_SURFACE, 0, 0, width, height);
	area_height = height;
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT);

	lp_cartesian_draw_start(cartesian);

	if (show_line) {
		lp_cartesian_draw_line(cartesian, line);
	}

	lp_cartesian_draw_end(cartesian);

	glutSwapBuffers();
}

void mouse_hover(int px, int py)
{
	float x, y;
	py = area_height - py; // mouse coordinate is top left, convert to bottom left
	lp_cartesian_pixel_to_data_coord(cartesian, px, py, &x, &y);
	show_line = isfinite(x) && isfinite(y);
	if (show_line) {
		lp_line_2float(line, LP_LINE_START, x, -INFINITY);
		lp_line_2float(line, LP_LINE_END, x, +INFINITY);
	}

	glutPostRedisplay();
}

void free_resources() {
	lp_cartesian_del(cartesian);
	lp_line_del(line);
}

int main(int argc, char *argv[]) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(640, 480);
	glutCreateWindow("libreplot");

	lp_enum glew_status = glewInit();

	if (GLEW_OK != glew_status) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0) {
		fprintf(stderr, "No support for OpenGL 2.0 found\n");
		return 1;
	}

	if (init_resources()) {
		glutReshapeFunc(window_resize);
		glutDisplayFunc(display);
		glutMotionFunc(mouse_hover);
		glutPassiveMotionFunc(mouse_hover);
		glutMainLoop();
	}

	free_resources();
	return 0;
}
