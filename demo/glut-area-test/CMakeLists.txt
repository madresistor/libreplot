# Make sure the compiler can find include files from our Hello library.
#include_directories (${HELLO_SOURCE_DIR}/Hello)

# Make sure the linker can find the Hello library once it is built.
LINK_DIRECTORIES (${BOX0_BINARY_DIR}/libreplot)

INCLUDE_DIRECTORIES(${PROJECT_BINARY_DIR}/libreplot ${PROJECT_SOURCE_DIR}/libreplot)

#GLUT
FIND_PACKAGE(GLUT REQUIRED)
INCLUDE_DIRECTORIES(${GLUT_INCLUDE_DIR})

#GLEW
FIND_PACKAGE(GLEW REQUIRED)
INCLUDE_DIRECTORIES(${GLEW_INCLUDE_DIRS})

#OpenGL
FIND_PACKAGE(OpenGL REQUIRED)
INCLUDE_DIRECTORIES(${OPENGL_INCLUDE_DIRS})

# Add executable called "helloDemo" that is built from the source files
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR})
ADD_EXECUTABLE (glut-area-test main.c)

# Link the executable to the Hello library.
TARGET_LINK_LIBRARIES (glut-area-test replot)
TARGET_LINK_LIBRARIES(glut-area-test ${GLUT_LIBRARIES})
TARGET_LINK_LIBRARIES(glut-area-test ${GLEW_LIBRARIES})
TARGET_LINK_LIBRARIES(glut-area-test ${OPENGL_LIBRARIES})
