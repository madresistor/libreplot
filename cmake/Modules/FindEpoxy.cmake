# - Try to find Epoxy
#
#  EPOXY_FOUND - system has Epoxy
#  EPOXY_INCLUDE_DIRS - the Epoxy include directory
#  EPOXY_LIBRARIES - Link these to use Epoxy
#  EPOXY_DEFINITIONS - Compiler switches required for using Epoxy
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(EPOXY_INCLUDE_DIR
	NAMES epoxy/gl.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "Epoxy.so"
FIND_LIBRARY(EPOXY_LIBRARY
	NAMES epoxy libepoxy
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (EPOXY_INCLUDE_DIR AND EPOXY_LIBRARY)
	SET(EPOXY_FOUND TRUE)
	SET(EPOXY_INCLUDE_DIRS ${EPOXY_INCLUDE_DIR})
	SET(EPOXY_LIBRARIES ${EPOXY_LIBRARY})
ENDIF (EPOXY_INCLUDE_DIR AND EPOXY_LIBRARY)

IF (EPOXY_FOUND)
	IF (NOT Epoxy_FIND_QUIETLY)
		MESSAGE(STATUS "Found Epoxy:")
		MESSAGE(STATUS " - Includes: ${EPOXY_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${EPOXY_LIBRARIES}")
	ENDIF (NOT Epoxy_FIND_QUIETLY)
ELSE (EPOXY_FOUND)
	IF (Epoxy_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find Epoxy")
	ENDIF (Epoxy_FIND_REQUIRED)
ENDIF (EPOXY_FOUND)

# show the EPOXY_INCLUDE_DIRS and EPOXY_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(EPOXY_INCLUDE_DIRS EPOXY_LIBRARIES)
