# - Try to find GLESv3
#
#  GLESv3_FOUND - system has GLESv3
#  GLESv3_INCLUDE_DIRS - the GLESv3 include directory
#  GLESv3_LIBRARIES - Link these to use GLESv3
#  GLESv3_DEFINITIONS - Compiler switches required for using GLESv3
#
#  Adapted from cmake-modules Google Code project
#
#  Copyright (c) 2006 Andreas Schneider <mail@cynapses.org>
#  Copyright (c) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
#
# Redistribution and use is allowed according to the terms of the New BSD license.
#
# CMake-Modules Project New BSD License
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the CMake-Modules Project nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

FIND_PATH(GLESv3_INCLUDE_DIR
	NAMES GLES3/gl3.h
	PATHS
		/usr/include
		/usr/local/include
		/opt/local/include
		/sw/include
)

# Note: Only searching for "GLESv3.so"
FIND_LIBRARY(GLESv3_LIBRARY
	NAMES GLESv3 libGLESv3
	PATHS
		/usr/lib
		/usr/local/lib
		/opt/local/lib
		/sw/lib
)

IF (GLESv3_INCLUDE_DIR AND GLESv3_LIBRARY)
	SET(GLESv3_FOUND TRUE)
	SET(GLESv3_INCLUDE_DIRS ${GLESv3_INCLUDE_DIR})
	SET(GLESv3_LIBRARIES ${GLESv3_LIBRARY})
ENDIF (GLESv3_INCLUDE_DIR AND GLESv3_LIBRARY)

IF (GLESv3_FOUND)
	IF (NOT GLESv3_FIND_QUIETLY)
		MESSAGE(STATUS "Found GLESv3:")
		MESSAGE(STATUS " - Includes: ${GLESv3_INCLUDE_DIRS}")
		MESSAGE(STATUS " - Libraries: ${GLESv3_LIBRARIES}")
	ENDIF (NOT GLESv3_FIND_QUIETLY)
ELSE (GLESv3_FOUND)
	IF (GLESv3_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find GLESv3")
	ENDIF (GLESv3_FIND_REQUIRED)
ENDIF (GLESv3_FOUND)

# show the GLESv3_INCLUDE_DIRS and GLESv3_LIBRARIES variables only in the advanced view
MARK_AS_ADVANCED(GLESv3_INCLUDE_DIRS GLESv3_LIBRARIES)
