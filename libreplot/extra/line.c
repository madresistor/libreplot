/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "line-private.h"

static struct lp_line DEFAULT = {
	.color = LP_COLOR_BLACK,
	.width = 1,
	.start = {0, 0},
	.end = {0, 0}
};

lp_line *lp_line_gen()
{
	lp_line *line = malloc(sizeof(*line));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(line, NULL);
	*line = DEFAULT;
	lp_obj_manager_init(&line->obj_manager);
	return line;
}

OBJ_REF_FUNC(line)

void lp_line_del(lp_line *line)
{
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	if (lp_obj_manager_fini(&line->obj_manager)) {
		free(line);
	}
}

void lp_line_float(lp_line *line, lp_line_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	switch (part) {
	case LP_LINE_WIDTH:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
		line->width = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_line_2float(lp_line *line, lp_line_part part, float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	switch (part) {
	case LP_LINE_START:
		line->start = (lp_vec2f){arg0, arg1};
	break;
	case LP_LINE_END:
		line->end = (lp_vec2f){arg0, arg1};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_line_4float(lp_line *line, lp_line_part part,
		float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	switch (part) {
	case LP_LINE_COLOR:
		line->color = (lp_color4f){arg0, arg1, arg2, arg3};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}
