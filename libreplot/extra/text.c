/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "text-private.h"

static const lp_text DEFAULT = {
	.font = NULL,
	.string = NULL,
	.position = (lp_vec2f){0, 0},
	.align = LP_ALIGN_RIGHT | LP_ALIGN_TOP,
	.rotate = 0,
	.height = 8,
	.color = LP_COLOR_BLACK
};

lp_text *lp_text_gen(void)
{
	lp_text *text = malloc(sizeof(*text));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(text, NULL);
	*text = DEFAULT;
	lp_obj_manager_init(&text->obj_manager);
	return text;
}

OBJ_REF_FUNC(text)

void lp_text_del(lp_text *text)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	if (lp_obj_manager_fini(&text->obj_manager)) {
		DEL_OBJ(text->font, lp_font_del);
		free(text);
	}
}

void lp_text_float(lp_text *text, lp_text_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	switch(part) {
	case LP_TEXT_HEIGHT:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
		text->height = arg0;
	break;
	case LP_TEXT_ROTATE:
		text->rotate = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_text_2float(lp_text *text, lp_text_part part, float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	switch(part) {
	case LP_TEXT_POSITION:
		text->position = (lp_vec2f){arg0, arg1};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_text_4float(lp_text *text, lp_text_part part,
				float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	switch(part) {
	case LP_TEXT_COLOR:
		text->color = (lp_color4f){arg0, arg1, arg2, arg3};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_text_enum(lp_text *text, lp_text_part part, lp_enum arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	switch(part) {
	case LP_TEXT_ALIGN:
		text->align = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_text_pointer(lp_text *text, lp_text_part part, void *arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	switch(part) {
	case LP_TEXT_FONT:
		REPLACE_FONT(text->font, arg0);
	break;
	case LP_TEXT_STRING:
		REPLACE_STR(text->string, arg0);
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}
