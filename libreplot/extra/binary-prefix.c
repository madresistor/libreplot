/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "binary-prefix-private.h"

static const lp_binary_prefix_item items[] = {
	{CONST_UTF8_STR("-"), CONST_UTF8_STR(""), 0},
	{CONST_UTF8_STR("kibi"), CONST_UTF8_STR("Ki"), 10},
	{CONST_UTF8_STR("mebi"), CONST_UTF8_STR("Mi"), 20},
	{CONST_UTF8_STR("gibi"), CONST_UTF8_STR("Gi"), 30},
	{CONST_UTF8_STR("tebi"), CONST_UTF8_STR("Ti"), 40},
	{CONST_UTF8_STR("pebi"), CONST_UTF8_STR("Pi"), 50},
	{CONST_UTF8_STR("exbi"), CONST_UTF8_STR("Ei"), 60},
	{CONST_UTF8_STR("zebi"), CONST_UTF8_STR("Zi"), 70},
	{CONST_UTF8_STR("yobi"), CONST_UTF8_STR("Yi"), 80},
	{NULL, NULL, 0}
};

#define COMMONLY_USED_ENTRIES_LEN 9
static const lp_binary_prefix_entry
commonly_used_entries[COMMONLY_USED_ENTRIES_LEN] = {
	LP_BINARY_PREFIX_NONE,
	LP_BINARY_PREFIX_KIBI,
	LP_BINARY_PREFIX_MEBI,
	LP_BINARY_PREFIX_GIBI,
	LP_BINARY_PREFIX_TEBI,
	LP_BINARY_PREFIX_PEBI,
	LP_BINARY_PREFIX_EXBI,
	LP_BINARY_PREFIX_ZEBI,
	LP_BINARY_PREFIX_YOBI,
};

lp_binary_prefix *lp_binary_prefix_commonly_used(void)
{
	static lp_binary_prefix *commonly_used = NULL;

	if (commonly_used == NULL) {
		/* lazy loading */
		commonly_used = lp_binary_prefix_gen();
		LOG_ASSERT_RET_ON_NULL(commonly_used, NULL);
		lp_binary_prefix_add(commonly_used,
			commonly_used_entries, COMMONLY_USED_ENTRIES_LEN);
	}

	return commonly_used;
}

lp_binary_prefix *lp_binary_prefix_gen(void)
{
	lp_binary_prefix *binary_prefix = malloc(sizeof(*binary_prefix));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(binary_prefix, NULL);
	binary_prefix->len = 0;
	binary_prefix->list = NULL;
	lp_obj_manager_init(&binary_prefix->obj_manager);
	return binary_prefix;
}

OBJ_REF_FUNC(binary_prefix)

void lp_binary_prefix_del(lp_binary_prefix *binary_prefix)
{
	LOG_ASSERT_RET_ON_NULL_ARG(binary_prefix);

	if (lp_obj_manager_fini(&binary_prefix->obj_manager)) {
		free(binary_prefix);
	}
}

static const lp_binary_prefix_item *search_item(int power)
{
	const lp_binary_prefix_item *i;

	for (i = items; i->name != NULL; i++) {
		if (i->power == power) {
			return i;
		}
	}

	return NULL;
}

void lp_binary_prefix_add(lp_binary_prefix *binary_prefix,
	const lp_binary_prefix_entry *entries, size_t len)
{
	size_t i, next;
	const lp_binary_prefix_item **list;

	LOG_ASSERT_RET_ON_NULL_ARG(binary_prefix);
	LOG_ASSERT_RET_ON_NULL_ARG(entries);
	LOG_ASSERT_RET_ON_FAIL_ARG(len > 0);

	lp_obj_manager_lock(&binary_prefix->obj_manager);

	list = realloc(binary_prefix->list,
		sizeof(*binary_prefix->list) * (len + binary_prefix->len));
	if (list == NULL) {
		LOG_WARN("failed to allocate memory for binary prefix list");
		goto unlock;
	}

	for (i = 0, next = binary_prefix->len; i < len; i++) {
		const lp_binary_prefix_item *item = search_item(entries[i]);
		if (item == NULL) {
			LOG_WARN("item with power %"PRIuS" not found", i);
			continue;
		}

		/* store the item */
		list[next++] = item;
	}

	/* TODO: realloc if not utilizing the full allocated memory. */
	binary_prefix->len = next;
	binary_prefix->list = list;

	unlock:
	lp_obj_manager_unlock(&binary_prefix->obj_manager);
}
