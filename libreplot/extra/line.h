/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_LINE_H
#define LIBREPLOT_EXTRA_LINE_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_line lp_line;

LP_API lp_line *lp_line_gen(void);
LP_API void lp_line_ref(lp_line *line);
LP_API void lp_line_del(lp_line *line);

enum lp_line_part {
	/* float */
	LP_LINE_WIDTH = 0,

	/* 2float */
	LP_LINE_START = 10, /* x, y */
	LP_LINE_END = 11,

	/* 4float */
	LP_LINE_COLOR = 20
};

typedef enum lp_line_part lp_line_part;

LP_API void lp_line_float(lp_line *line, lp_line_part part, float arg0);
LP_API void lp_line_2float(lp_line *line, lp_line_part part, float arg0, float arg1);
LP_API void lp_line_4float(lp_line *line, lp_line_part part,
		float arg0, float arg1, float arg2, float arg3);

__END_DECLS

#endif
