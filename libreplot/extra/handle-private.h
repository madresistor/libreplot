/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_HANDLE_PRIVATE_H
#define LIBREPLOT_EXTRA_HANDLE_PRIVATE_H

#include "../common.h"
#include "obj-manager-private.h"
#include "font-private.h"
#include "handle.h"

__BEGIN_DECLS

struct lp_handle {
	lp_obj_manager obj_manager;

	struct {
		struct {
			float height, width;
		} min, max;
		struct {
			float angle;
		} nose;
	} dragger;

	struct {
		bool show;
		lp_font *font;
		float height;
	} text;

	struct {
		float width;
	} line;
};

__END_DECLS

#endif
