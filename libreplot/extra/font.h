/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_FONT_H
#define LIBREPLOT_EXTRA_FONT_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_font lp_font;

/**
 * Generate a font object from file. (see freetype2)
 * @param file File path
 * @return font object (NULL if failed to process file)
 */
LP_API lp_font *lp_font_gen_file(const char *file);

/**
 * Generate font object from font data stored in memory
 * @param mem Pointer to data
 * @param bytes Number of bytes in @a mem
 * @param free Function to call when memory is done. \n
 *   if free is NULL, the ownership to font object is not passed.
 * @return font object
 */
lp_font *lp_font_gen_mem(void *mem, size_t bytes, lp_free_cb free_cb);

/**
 * Generate font object via custom read function
 * @param user_data user_data argument for read_fn
 * @param total_len total length of buffer that is required (maximum)
 * @param read_fn Read function
 * @return font object
 */
LP_API lp_font *lp_font_gen_read(void *user_data, size_t total_len, lp_io_cb read_fn);

/**
 * Generate a font object.
 *  This will be probebly some system default font that you can use without much headache.
 * This will return a font that is generate once.
 * @return font object
 */
LP_API lp_font *lp_font_gen(void);

/**
 * Increase reference to object
 * @param font object
 */
LP_API void lp_font_ref(lp_font *font);

/**
 * delete the object.
 *  It will only be deleted if the reference count goes 0.
 * @param font object
 */
LP_API void lp_font_del(lp_font *font);

__END_DECLS

#endif
