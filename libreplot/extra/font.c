/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "font-private.h"

/**
 * if LP_FONT_DEFAULT_PATH is defined, then the path is used as default and
 *  Fontconfig is not used (runtime) for font detection.
 *
 * If you need to search for font at runtime,
 *  you can use Fontconfig API directly.
 * You can use runtime_get_default_font() code as starting point.
 */

#if !defined(LP_FONT_DEFAULT_PATH)
# include <fontconfig/fontconfig.h>

/* Ref: http://stackoverflow.com/a/14634033/1500988 */

/**
 * Get a initalized font config object.
 *  This will perform lazy loading
 * @return the initalized font config object
 * @warning no thread safe
 */
static FcConfig *get_fc_config(void)
{
	static FcConfig *config = NULL;

	if (config != NULL) {
		return config;
	}

	LOG_ASSERT_RET_ON_FAIL(FcInit(), NULL);
	config = FcInitLoadConfigAndFonts();
	LOG_WARN_IF_NULL(config);
	return config;
}

static lp_font *runtime_get_default_font(void)
{
	lp_font *font_result = NULL;
	FcConfig *config = get_fc_config();
	LOG_ASSERT_RET_ON_FAIL(config, NULL);

	/* build query */
	FcPattern* pat = FcNameParse((FcChar8 *) "");
	LOG_WARN_IF_NULL(pat);
	FcPatternAddInteger(pat, FC_WEIGHT, FC_WEIGHT_NORMAL);
	FcPatternAddInteger(pat, FC_SLANT, FC_SLANT_ROMAN);
	FcConfigSubstitute(config, pat, FcMatchPattern);
	FcDefaultSubstitute(pat);

	/* find the font */
	FcResult result;
	FcPattern *font = FcFontMatch(config, pat, &result);
	LOG_ASSERT_RET_ON_NULL(font, NULL);
	LOG_ASSERT_RET_ON_FAIL(result == FcResultMatch, NULL);

	if (font != NULL) {
		FcChar8 *file = NULL;
		if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch) {
			font_result = lp_font_gen_file((const char *) file);
		}
		FcPatternDestroy(font);
	}

	FcPatternDestroy(pat);

	return font_result;
}

#endif

/**
 * Get a initalize Freetype library object.
 *  This will perform lazy loading.
 * @return freetype library object
 * @warning not thread safe
 */
static FT_Library get_ft_lib(void)
{
	static FT_Library ft_lib = NULL;

	if (ft_lib == NULL) {
		if (FT_Init_FreeType(&ft_lib)) {
			fprintf(stderr, "Could not init freetype library\n");
			return NULL;
		}
	}

	return ft_lib;
}

/**
 * Convert ft_face into a font object.
 * @param ft_face Freetype face object (ownership is passed to font object)
 * @param ref_mem Reference memory (can be NULL)
 * @param ref_free function to be called (with argument @a ref_mem)
 *    when the font object is deleted.
 *    This should be NULL if ref_mem is NULL.
 * @return font object
 */
static lp_font *to_font(FT_Face ft_face, void *ref_mem, lp_free_cb ref_free)
{
	hb_font_t *hb_font;
	lp_font *font;

	hb_font = hb_ft_font_create(ft_face, NULL);
	if (hb_font == NULL) {
		FT_Done_Face(ft_face);
		LOG_WARN("Could not initalize harfbuzz font");
		return NULL;
	}

	font = malloc(sizeof(*font));
	if (font == NULL) {
		hb_font_destroy(hb_font);
		FT_Done_Face(ft_face);
	}
	LOG_ASSERT_RET_ON_ALLOC_FAIL(font, NULL);

	font->hb_font = hb_font;
	font->ft_face = ft_face;
	font->ref_mem = ref_mem;
	font->ref_free = ref_free;

	lp_obj_manager_init(&font->obj_manager);

	return font;
}

lp_font *lp_font_gen_file(const char *file)
{
	FT_Face ft_face;
	if (FT_New_Face(get_ft_lib(), file, 0, &ft_face)) {
		LOG_WARN("Could not open font %s", file);
		return NULL;
	}

	return to_font(ft_face, NULL, NULL);
}

lp_font *lp_font_gen_mem(void *mem, size_t bytes, lp_free_cb free_cb)
{
	LOG_ASSERT_RET_ON_ALLOC_FAIL(mem, NULL);
	LOG_ASSERT_RET_ON_FAIL(bytes > 0, NULL);

	FT_Face ft_face;

	if (free_cb == NULL) {
		void *bkp = mem;
		mem = malloc(bytes);
		LOG_ASSERT_RET_ON_ALLOC_FAIL(mem, NULL);
		memcpy(mem, bkp, bytes);
		free_cb = free;
	}

	if (FT_New_Memory_Face(get_ft_lib(), mem, bytes, 0, &ft_face)) {
		LOG_WARN("Could not parse font data");
		return NULL;
	}

	return to_font(ft_face, mem, free_cb);
}

lp_font *lp_font_gen(void)
{
	static lp_font *def = NULL;

	if (def == NULL) {
#if defined(LP_FONT_DEFAULT_PATH)
		def = lp_font_gen_file(LP_FONT_DEFAULT_PATH);
#else
		def = runtime_get_default_font();
#endif
	}

	/* make a reference for the caller */
	lp_font_ref(def);

	return def;
}

OBJ_REF_FUNC(font)

void lp_font_del(lp_font *font)
{
	LOG_ASSERT_RET_ON_NULL_ARG(font);

	if (lp_obj_manager_fini(&font->obj_manager)) {
		hb_font_destroy(font->hb_font);
		FT_Done_Face(font->ft_face);

		if (font->ref_mem != NULL) {
			/* NOTE: here assumed that the free function will always be non-NULL
			 *  if ref_mem is not NULL */
			font->ref_free(font->ref_mem);
		}

		free(font);
	}
}

lp_font *lp_font_gen_read(void *user_data, size_t total_len, lp_io_cb read_cb)
{
	const size_t one_packet = 4096;
	size_t i;
	void *buf;

	LOG_ASSERT_RET_ON_FAIL(total_len > 0, NULL);
	buf = malloc(total_len);

	for (i = 0; i < total_len; i += one_packet) {
		size_t readed = read_cb(user_data, buf + i, one_packet);
		if (readed < one_packet) {
			i += readed;
			break;
		}
	}

	LOG_ASSERT_RET_ON_FAIL(i > 0, NULL);
	return lp_font_gen_mem(buf, i, free);
}
