/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "circle-private.h"

/* default value of circle */
static const lp_circle DEFAULT = {
	.radius = 1,
	.center = {0, 0},

	.line = {
		.color = LP_COLOR_BLACK,
		.width = 1,
		.show = true
	},

	.fill = {
		.color = LP_COLOR_BLACK,
		.show = false
	},

	.approx_arc = 0.01
};

lp_circle *lp_circle_gen(void)
{
	lp_circle *circle = malloc(sizeof(*circle));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(circle, NULL);
	*circle = DEFAULT;
	lp_obj_manager_init(&circle->obj_manager);
	return circle;
}

OBJ_REF_FUNC(circle)

void lp_circle_del(lp_circle *circle)
{
	LOG_ASSERT_RET_ON_NULL_ARG(circle);
	if (lp_obj_manager_fini(&circle->obj_manager)) {
		free(circle);
	}
}

void lp_circle_bool(lp_circle *circle, lp_circle_part part, bool arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	switch (part) {
	case LP_CIRCLE_LINE_SHOW:
		circle->line.show = arg0;
	break;
	case LP_CIRCLE_FILL_SHOW:
		circle->fill.show = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_circle_float(lp_circle *circle, lp_circle_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	switch (part) {
	case LP_CIRCLE_APPROX_ARC:
	case LP_CIRCLE_RADIUS:
	case LP_CIRCLE_LINE_WIDTH:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
	break;
	default:
		/* nothing to do*/
	break;
	}

	switch (part) {
	case LP_CIRCLE_APPROX_ARC:
		circle->approx_arc = arg0;
	break;
	case LP_CIRCLE_RADIUS:
		circle->radius = arg0;
	break;
	case LP_CIRCLE_LINE_WIDTH:
		circle->line.width = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_circle_2float(lp_circle *circle, lp_circle_part part,
			float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	switch (part) {
	case LP_CIRCLE_CENTER:
		circle->center = (lp_vec2f){arg0, arg1};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_circle_4float(lp_circle *circle, lp_circle_part part,
			float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	lp_color4f color = {arg0, arg1, arg2, arg3};

	switch (part) {
	case LP_CIRCLE_FILL_COLOR:
		circle->fill.color = color;
	break;
	case LP_CIRCLE_LINE_COLOR:
		circle->line.color = color;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}
