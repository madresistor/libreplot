/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_RECTANGLE_H
#define LIBREPLOT_EXTRA_RECTANGLE_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_rectangle lp_rectangle;

/* rect management */
LP_API lp_rectangle *lp_rectangle_gen(void);
LP_API void lp_rectangle_ref(lp_rectangle *rect);
LP_API void lp_rectangle_del(lp_rectangle *rect);

enum lp_rectangle_part {
	/* bool */
	LP_RECTANGLE_LINE_SHOW = 0,
	LP_RECTANGLE_FILL_SHOW = 1,

	/* float */
	LP_RECTANGLE_LINE_WIDTH = 10,

	/* 4float */
	LP_RECTANGLE_LINE_COLOR = 20,
	LP_RECTANGLE_FILL_COLOR = 21,
	LP_RECTANGLE_GEOMETRY = 22 /* x, y, width, height */
};

typedef enum lp_rectangle_part lp_rectangle_part;

LP_API void lp_rectangle_bool(lp_rectangle *rect, lp_rectangle_part part, bool arg0);
LP_API void lp_rectangle_float(lp_rectangle *rect, lp_rectangle_part part, float arg0);
LP_API void lp_rectangle_4float(lp_rectangle *rect, lp_rectangle_part part,
			float arg0, float arg1, float arg2, float arg3);

__END_DECLS

#endif
