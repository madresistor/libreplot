/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "obj-manager-private.h"

/**
 * Initalize object manager.
 * @param obj_manager Object Manager
 * @return true on success
 */
bool lp_obj_manager_init(lp_obj_manager *obj_manager)
{
	obj_manager->ref = 1;
	obj_manager->renew_tag = 0;

	if (pthread_mutex_init(&obj_manager->lock, NULL)) {
		LOG_WARN("unable to init obj_manager lock");
		return false;
	}

	return true;
}

/**
 * hold mutual exclusion lock
 * @param obj_manager Object Manager
 * @return true on success
 */
bool lp_obj_manager_lock(lp_obj_manager *obj_manager)
{
	if (pthread_mutex_lock(&obj_manager->lock)) {
		LOG_WARN("unable to hold obj_manager lock");
		return false;
	}

	return true;
}

/**
 * release mutual exclusion lock
 * @param obj_manager Object Manager
 * @return true on success
 */
bool lp_obj_manager_unlock(lp_obj_manager *obj_manager)
{
	if (pthread_mutex_unlock(&obj_manager->lock)) {
		LOG_WARN("unable to release obj_manager lock");
		return false;
	}

	return true;
}

/**
 * delete Object manager.
 * @param obj_manager Object Manager
 * @return true if the object can be delete (free'd)
 */
bool lp_obj_manager_fini(lp_obj_manager *obj_manager)
{
	lp_obj_manager_lock(obj_manager);

	LOG_WARN_IF_FAIL(obj_manager->ref > 0);
	obj_manager->ref--;

	/* still reference exists */
	if (obj_manager->ref > 0) {
		lp_obj_manager_unlock(obj_manager);
		return false;
	}

	lp_obj_manager_unlock(obj_manager);
	pthread_mutex_destroy(&obj_manager->lock);
	return true;
}

/**
 * Increment the object manager reference
 * @param obj_manager Object Manager
 * @return true on success
 */
bool lp_obj_manager_ref(lp_obj_manager *obj_manager)
{
	lp_obj_manager_lock(obj_manager);

	LOG_WARN_IF_FAIL(obj_manager->ref > 0);
	obj_manager->ref++;

	lp_obj_manager_unlock(obj_manager);

	return true;
}

/**
 * In EGL, context can be lost anytime.
 * So, using a tag, the last time of renew is captured.
 * All object will pass the tag to there sub-object which can also be inited
 * @param obj_manager Object Manager
 * @param tag Tag that contain if the call is newer than last call.
 * Application code will mantain a tag and increment
 *  it everytime the EGL context is lost (and perform renew of objects).
 */
bool lp_obj_manager_renew(lp_obj_manager *obj_manager, unsigned tag)
{
	/* tag=0 is never expected because for tag=0
	 *  the context is created for first time
	 */
	LOG_WARN_IF_FAIL(tag > 0);

	/* newer than last call */
	if (tag > obj_manager->renew_tag) {
		obj_manager->renew_tag = tag;
		return true;
	}

	return false;
}
