/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_BINARY_PREFIX_H
#define LIBREPLOT_EXTRA_BINARY_PREFIX_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_binary_prefix lp_binary_prefix;

enum lp_binary_prefix_entry {
	LP_BINARY_PREFIX_NONE = 0,
	LP_BINARY_PREFIX_KIBI = 10, /* Ki */
	LP_BINARY_PREFIX_MEBI = 20, /* Mi */
	LP_BINARY_PREFIX_GIBI = 30, /* Gi */
	LP_BINARY_PREFIX_TEBI = 40, /* Ti */
	LP_BINARY_PREFIX_PEBI = 50, /* Pi */
	LP_BINARY_PREFIX_EXBI = 60, /* Ei */
	LP_BINARY_PREFIX_ZEBI = 70, /* Zi */
	LP_BINARY_PREFIX_YOBI = 80, /* Yi */
};

typedef enum lp_binary_prefix_entry lp_binary_prefix_entry;

LP_API lp_binary_prefix *lp_binary_prefix_gen(void);
LP_API void lp_binary_prefix_ref(lp_binary_prefix *binary_prefix);
LP_API void lp_binary_prefix_del(lp_binary_prefix *binary_prefix);

LP_API void lp_binary_prefix_add(lp_binary_prefix *binary_prefix,
	const lp_binary_prefix_entry *entries, size_t len);

/* WARNING: do not modify the returned list. */
LP_API lp_binary_prefix *lp_binary_prefix_commonly_used(void);

__END_DECLS

#endif
