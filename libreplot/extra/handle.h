/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_HANDLE_H
#define LIBREPLOT_EXTRA_HANDLE_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_handle lp_handle;

LP_API lp_handle *lp_handle_gen(void);
LP_API void lp_handle_ref(lp_handle *handle);
LP_API void lp_handle_del(lp_handle *handle);

enum lp_handle_part {
	/* bool */
	LP_HANDLE_TEXT_SHOW = 0,

	/* float */
	LP_HANDLE_DRAGGER_HEIGHT_MIN = 10,
	LP_HANDLE_DRAGGER_WIDTH_MIN = 11,
	LP_HANDLE_DRAGGER_HEIGHT_MAX = 12,
	LP_HANDLE_DRAGGER_WIDTH_MAX = 13,
	LP_HANDLE_LINE_WIDTH = 14,
	LP_HANDLE_TEXT_HEIGHT = 15,
	LP_HANDLE_DRAGGER_NOSE_ANGLE = 16, /* radian */

	/* pointer: lp_font */
	LP_HANDLE_TEXT_FONT = 20,
};

typedef enum lp_handle_part lp_handle_part;

LP_API void lp_handle_bool(lp_handle *handle, lp_handle_part part, bool arg0);
LP_API void lp_handle_float(lp_handle *handle, lp_handle_part part, float arg0);
LP_API void lp_handle_pointer(lp_handle *handle, lp_handle_part part, void *arg0);

__END_DECLS

#endif
