/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "nice-ticks-private.h"
#include <math.h>

/* Reference:
 *  Book: "Graphics Gems", Academic Press, 1990
 *  Author: Paul Heckbert
 *  Section:  Nice Numbers for Graph Labels
 */

/* TODO: glibc provide a implementatio of pow10 */
#define pow10(x) pow(10, x)

static double nice_round(double frac)
{
	if (frac <= 1) {
		frac = 1;
	} else if (frac < 3) {
		frac = 2;
	} else if (frac < 7) {
		frac = 5;
	} else {
		frac = 10;
	}

	return frac;
}

/**
 * @param fraction_shaper function that convert fraction of 0-10 to (1, 2, 5, 10)
 */
static double nice_numbers(double value, double (*fraction_shaper)(double))
{
	/* extract the multiplier of 10 from value */
	double multi_10 = pow10(floor(log10(value)));

	/* get the fraction (after removing the multiplier of 10's) */
	return fraction_shaper(value / multi_10) * multi_10;
}

void lp_nice_ticks(lp_nice_ticks_input *input, lp_nice_ticks_output *output)
{
	double spacing, range;

	output->min = output->max = output->inc = 0;

	LOG_ASSERT_RET_ON_FAIL_ARG(input->min < input->max);
	LOG_ASSERT_RET_ON_FAIL_ARG(input->ticks > 1);

	range = nice_numbers(input->max - input->min, round);
	spacing = nice_numbers(range / (input->ticks - 1), nice_round);

	output->min = floor(input->min / spacing) * spacing;
	output->max = (ceil(input->max / spacing) + 0.5) * spacing;
	output->inc = spacing;
}
