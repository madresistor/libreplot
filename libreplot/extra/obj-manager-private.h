/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_OBJ_MANAGER_H
#define LIBREPLOT_EXTRA_OBJ_MANAGER_H

#include "../common.h"
#include <pthread.h>

__BEGIN_DECLS

struct lp_obj_manager {
	pthread_mutex_t lock;
	int ref;
	unsigned renew_tag;
};

typedef struct lp_obj_manager lp_obj_manager;

LP_PRIV bool lp_obj_manager_init(lp_obj_manager *obj_manager);
LP_PRIV bool lp_obj_manager_lock(lp_obj_manager *obj_manager);
LP_PRIV bool lp_obj_manager_unlock(lp_obj_manager *obj_manager);
LP_PRIV bool lp_obj_manager_fini(lp_obj_manager *obj_manager);
LP_PRIV bool lp_obj_manager_ref(lp_obj_manager *obj_manager);
LP_PRIV bool lp_obj_manager_renew(lp_obj_manager *obj_manager, unsigned tag);

__END_DECLS

#endif
