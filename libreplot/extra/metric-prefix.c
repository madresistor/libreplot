/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "metric-prefix-private.h"

static const lp_metric_prefix_item items[] = {
	{CONST_UTF8_STR("yocto"), CONST_UTF8_STR("y"), -24},
	{CONST_UTF8_STR("zepto"), CONST_UTF8_STR("z"), -21},
	{CONST_UTF8_STR("atto"), CONST_UTF8_STR("a"), -18},
	{CONST_UTF8_STR("femto"), CONST_UTF8_STR("f"), -15},
	{CONST_UTF8_STR("pico"), CONST_UTF8_STR("p"), -12},
	{CONST_UTF8_STR("nano"), CONST_UTF8_STR("n"), -9},
	{CONST_UTF8_STR("micro"), CONST_UTF8_STR("μ"), -6},
	{CONST_UTF8_STR("milli"), CONST_UTF8_STR("m"), -3},
	{CONST_UTF8_STR("centi"), CONST_UTF8_STR("c"), -2}, /* not so common */
	{CONST_UTF8_STR("deci"), CONST_UTF8_STR("d"), -1}, /* not so common */
	{CONST_UTF8_STR("-"), CONST_UTF8_STR(""), 0},
	{CONST_UTF8_STR("deca"), CONST_UTF8_STR("da"), 1}, /* not so common */
	{CONST_UTF8_STR("hecto"), CONST_UTF8_STR("h"), 2}, /* not so common */
	{CONST_UTF8_STR("kilo"), CONST_UTF8_STR("k"), 3},
	{CONST_UTF8_STR("mega"), CONST_UTF8_STR("M"), 6},
	{CONST_UTF8_STR("giga"), CONST_UTF8_STR("G"), 9},
	{CONST_UTF8_STR("tera"), CONST_UTF8_STR("T"), 12},
	{CONST_UTF8_STR("peta"), CONST_UTF8_STR("P"), 15},
	{CONST_UTF8_STR("exa"), CONST_UTF8_STR("E"), 18},
	{CONST_UTF8_STR("zetta"), CONST_UTF8_STR("Z"), 21},
	{CONST_UTF8_STR("yotta"), CONST_UTF8_STR("Y"), 24},
	{NULL, NULL, 0}
};

#define COMMONLY_USED_ENTRIES_LEN 20
static const lp_metric_prefix_entry
commonly_used_entries[COMMONLY_USED_ENTRIES_LEN] = {
	LP_METRIC_PREFIX_YOCTO,
	LP_METRIC_PREFIX_ZEPTO,
	LP_METRIC_PREFIX_ATTO,
	LP_METRIC_PREFIX_FEMTO,
	LP_METRIC_PREFIX_PICO,
	LP_METRIC_PREFIX_NANO,
	LP_METRIC_PREFIX_MICRO,
	LP_METRIC_PREFIX_MILLI,
	LP_METRIC_PREFIX_NONE,
	LP_METRIC_PREFIX_KILO,
	LP_METRIC_PREFIX_MEGA,
	LP_METRIC_PREFIX_GIGA,
	LP_METRIC_PREFIX_TERA,
	LP_METRIC_PREFIX_PETA,
	LP_METRIC_PREFIX_EXA,
	LP_METRIC_PREFIX_ZETTA,
	LP_METRIC_PREFIX_YOTTA,
};

lp_metric_prefix *lp_metric_prefix_commonly_used(void)
{
	static lp_metric_prefix *commonly_used = NULL;

	if (commonly_used == NULL) {
		/* lazy loading */
		commonly_used = lp_metric_prefix_gen();
		LOG_ASSERT_RET_ON_NULL(commonly_used, NULL);
		lp_metric_prefix_add(commonly_used,
			commonly_used_entries, COMMONLY_USED_ENTRIES_LEN);
	}

	return commonly_used;
}

lp_metric_prefix *lp_metric_prefix_gen(void)
{
	lp_metric_prefix *metric_prefix = malloc(sizeof(*metric_prefix));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(metric_prefix, NULL);
	metric_prefix->len = 0;
	metric_prefix->list = NULL;
	lp_obj_manager_init(&metric_prefix->obj_manager);
	return metric_prefix;
}

OBJ_REF_FUNC(metric_prefix)

void lp_metric_prefix_del(lp_metric_prefix *metric_prefix)
{
	LOG_ASSERT_RET_ON_NULL_ARG(metric_prefix);

	if (lp_obj_manager_fini(&metric_prefix->obj_manager)) {
		free(metric_prefix);
	}
}

static const lp_metric_prefix_item *search_item(int power)
{
	const lp_metric_prefix_item *i;

	for (i = items; i->name != NULL; i++) {
		if (i->power == power) {
			return i;
		}
	}

	return NULL;
}

void lp_metric_prefix_add(lp_metric_prefix *metric_prefix,
	const lp_metric_prefix_entry *entries, size_t len)
{
	size_t i, next;
	const lp_metric_prefix_item **list;

	LOG_ASSERT_RET_ON_NULL_ARG(metric_prefix);
	LOG_ASSERT_RET_ON_NULL_ARG(entries);
	LOG_ASSERT_RET_ON_FAIL_ARG(len > 0);

	lp_obj_manager_lock(&metric_prefix->obj_manager);

	list = realloc(metric_prefix->list,
		sizeof(*metric_prefix->list) * (len + metric_prefix->len));
	if (list == NULL) {
		LOG_WARN("failed to allocate memory for metric prefix list");
		goto unlock;
	}

	for (i = 0, next = metric_prefix->len; i < len; i++) {
		const lp_metric_prefix_item *item = search_item(entries[i]);
		if (item == NULL) {
			LOG_WARN("item with power %"PRIuS" not found", i);
			continue;
		}

		/* store the item */
		list[next++] = item;
	}

	/* TODO: realloc if not utilizing the full allocated memory. */
	metric_prefix->len = next;
	metric_prefix->list = list;

	unlock:
	lp_obj_manager_unlock(&metric_prefix->obj_manager);
}
