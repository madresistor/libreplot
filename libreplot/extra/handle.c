/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "handle-private.h"

static const lp_handle DEFAULT = {
	.dragger = {
		.min = {
			.height = 1,
			.width = 1
		},

		.max = {
			.height = 1,
			.width = 1,
		},

		.nose = {
			.angle = M_PI_2
		}
	},

	.text = {
		.show = true,
		.font = NULL,
		.height = 9
	},

	.line = {
		.width = 2
	}
};

lp_handle *lp_handle_gen(void)
{
	lp_handle *handle = malloc(sizeof(*handle));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(handle, NULL);
	*handle = DEFAULT;
	lp_obj_manager_init(&handle->obj_manager);
	return handle;
}

OBJ_REF_FUNC(handle)

void lp_handle_del(lp_handle *handle)
{
	LOG_ASSERT_RET_ON_NULL_ARG(handle);
	if (lp_obj_manager_fini(&handle->obj_manager)) {
		DEL_OBJ(handle->text.font, lp_font_del);
		free(handle);
	}
}

void lp_handle_bool(lp_handle *handle, lp_handle_part part, bool arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(handle);

	switch(part) {
	case LP_HANDLE_TEXT_SHOW:
		handle->text.show = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_handle_float(lp_handle *handle, lp_handle_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(handle);

	/* check if greather than 0 */
	switch (part) {
	case LP_HANDLE_DRAGGER_HEIGHT_MIN:
	case LP_HANDLE_DRAGGER_HEIGHT_MAX:
	case LP_HANDLE_DRAGGER_WIDTH_MAX:
	case LP_HANDLE_DRAGGER_WIDTH_MIN:
	case LP_HANDLE_LINE_WIDTH:
	case LP_HANDLE_TEXT_HEIGHT:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
	break;
	default:
		/* nothing to do */
	break;
	}

	switch(part) {
	case LP_HANDLE_DRAGGER_HEIGHT_MIN:
		handle->dragger.min.height = arg0;
	break;
	case LP_HANDLE_DRAGGER_HEIGHT_MAX:
		handle->dragger.max.height = arg0;
	break;
	case LP_HANDLE_DRAGGER_WIDTH_MIN:
		handle->dragger.max.width = arg0;
	break;
	case LP_HANDLE_DRAGGER_WIDTH_MAX:
		handle->dragger.max.width = arg0;
	break;
	case  LP_HANDLE_LINE_WIDTH:
		handle->line.width = arg0;
	break;
	case LP_HANDLE_TEXT_HEIGHT:
		handle->text.height = arg0;
	break;
	case LP_HANDLE_DRAGGER_NOSE_ANGLE: /* radian */
		handle->dragger.nose.angle = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_handle_pointer(lp_handle *handle, lp_handle_part part, void *arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(handle);

	switch(part) {
	case LP_HANDLE_TEXT_FONT:
		REPLACE_FONT(handle->text.font, arg0);
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}
