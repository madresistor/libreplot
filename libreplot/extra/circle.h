/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_EXTRA_CIRCLE_H
#define LIBREPLOT_EXTRA_CIRCLE_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_circle lp_circle;

/* TODO: move these to other file */
typedef struct lp_circle_sector lp_circle_sector;
typedef struct lp_circle_segment lp_circle_segment;
typedef struct lp_circle_arc lp_circle_arc;

/* Circle management */
LP_API lp_circle *lp_circle_gen(void);
LP_API void lp_circle_ref(lp_circle *circle);
LP_API void lp_circle_del(lp_circle *circle);

enum lp_circle_part {
	/* bool */
	LP_CIRCLE_LINE_SHOW = 0,
	LP_CIRCLE_FILL_SHOW = 1,

	/* float */
	LP_CIRCLE_APPROX_ARC = 10,
	LP_CIRCLE_RADIUS = 11,
	LP_CIRCLE_LINE_WIDTH = 12,

	/* 2float */
	LP_CIRCLE_CENTER = 20, /* x, y */

	/* 4float */
	LP_CIRCLE_LINE_COLOR = 30,
	LP_CIRCLE_FILL_COLOR = 31,
};

typedef enum lp_circle_part lp_circle_part;

LP_API void lp_circle_bool(lp_circle *circle, lp_circle_part part, bool arg0);
LP_API void lp_circle_float(lp_circle *circle, lp_circle_part part, float arg0);
LP_API void lp_circle_2float(lp_circle *circle, lp_circle_part part,
			float arg0, float arg1);
LP_API void lp_circle_4float(lp_circle *circle, lp_circle_part part,
			float arg0, float arg1, float arg2, float arg3);

__END_DECLS

#endif
