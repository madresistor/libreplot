/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "axis-private.h"

static const lp_cartesian_axis DEFAULT = {
	.name = {
		.show = GL_FALSE,
		.color = LP_COLOR_BLACK,
		.value = NULL,
		.font = NULL,
		.height = 9
	},

	.handle = {
		.show = false,
		.design = NULL
	},

	.grid = {
		.show = true,
		.color = {0.8, 0.8, 0.8, 1},
		.width = 1
	},

	.baseline = {
		.show = true,
		.color = {0.4, 0.4, 0.4, 1},
		.width = 2
	},

	.tick = {
		.show = true,
		.color = LP_COLOR_BLACK,
		.width = 1,
		.length = 7
	},

	.value = {
		.show = true,
		.height = 9,
		.color = LP_COLOR_BLACK,
		.font = NULL,
		.append = NULL,
		.prepend = NULL,

		.max = 1,
		.min = -1
	},

	.prefix = {
		.select = LP_CARTESIAN_AXIS_PREFIX_SELECT_NONE,
		.metric = NULL,
		.binary = NULL
	},

	.unit = {
		.show = false,
		.value = NULL
	},

	.div = {
		.scale = LP_CARTESIAN_AXIS_SCALE_SELECT_LINEAR,
		.count = 0,

		/* if a very small value is given,
		 *  rudementry=true calculation will take alot of time.
		 * so, applications are recommended to not give smaller values than 10
		 * unit: mm
		 */
		.auto_spacing_hint = 10
	},
};

lp_cartesian_axis *lp_cartesian_axis_gen(void)
{
	lp_cartesian_axis *axis = malloc(sizeof(*axis));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(axis, NULL);
	*axis = DEFAULT;
	lp_obj_manager_init(&axis->obj_manager);
	return axis;
}

OBJ_REF_FUNC(cartesian_axis)

void lp_cartesian_axis_del(lp_cartesian_axis *axis)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	if (lp_obj_manager_fini(&axis->obj_manager)) {
		DEL_OBJ(axis->name.value, free);
		DEL_OBJ(axis->handle.design, lp_handle_del);
		DEL_OBJ(axis->value.font, lp_font_del);
		DEL_OBJ(axis->value.prepend, free);
		DEL_OBJ(axis->value.append, free);
		DEL_OBJ(axis->prefix.metric, lp_metric_prefix_del);
		DEL_OBJ(axis->prefix.binary, lp_binary_prefix_del);
		DEL_OBJ(axis->unit.value, free);
		free(axis);
	}
}

void lp_cartesian_axis_bool(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, bool arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_NAME_SHOW:
		axis->name.show = arg0;
	break;
	case LP_CARTESIAN_AXIS_BASELINE_SHOW:
		axis->baseline.show = arg0;
	break;
	case LP_CARTESIAN_AXIS_TICK_SHOW:
		axis->tick.show = arg0;
	break;
	case LP_CARTESIAN_AXIS_VALUE_SHOW:
		axis->value.show = arg0;
	break;
	case LP_CARTESIAN_AXIS_GRID_SHOW:
		axis->grid.show = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_float(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_NAME_HEIGHT:
	case LP_CARTESIAN_AXIS_VALUE_HEIGHT:
	case LP_CARTESIAN_AXIS_BASELINE_WIDTH:
	case LP_CARTESIAN_AXIS_TICK_WIDTH:
	case LP_CARTESIAN_AXIS_TICK_LENGTH:
	case LP_CARTESIAN_AXIS_DIV_AUTO_SPACING_HINT:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
	break;
	default:
		/* nothing to do */
	break;
	}

	switch (part) {
	case LP_CARTESIAN_AXIS_NAME_HEIGHT:
		axis->name.height = arg0;
	break;
	case LP_CARTESIAN_AXIS_VALUE_HEIGHT:
		axis->value.height = arg0;
	break;
	case LP_CARTESIAN_AXIS_BASELINE_WIDTH:
		axis->baseline.width = arg0;
	break;
	case LP_CARTESIAN_AXIS_TICK_WIDTH:
		axis->tick.width = arg0;
	break;
	case LP_CARTESIAN_AXIS_TICK_LENGTH:
		axis->tick.length = arg0;
	break;
	case LP_CARTESIAN_AXIS_DIV_AUTO_SPACING_HINT:
		axis->div.auto_spacing_hint = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_2float(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_VALUE_RANGE:
		LOG_ASSERT_RET_ON_FAIL(arg0 < arg1);
		axis->value.min = arg0;
		axis->value.max = arg1;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	break;
	}
}

void lp_get_cartesian_axis_2float(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, float *arg0, float *arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);
	LOG_ASSERT_RET_ON_NULL_ARG(arg0);
	LOG_ASSERT_RET_ON_NULL_ARG(arg1);

	switch (part) {
	case LP_CARTESIAN_AXIS_VALUE_RANGE:
		*arg0 = axis->value.min;
		*arg1 = axis->value.max;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	break;
	}
}

void lp_cartesian_axis_4float(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part,
			float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	lp_color4f color = {arg0, arg1, arg2, arg3};

	switch (part) {
	case LP_CARTESIAN_AXIS_NAME_COLOR:
		axis->name.color = color;
	break;
	case LP_CARTESIAN_AXIS_BASELINE_COLOR:
		axis->baseline.color = color;
	break;
	case LP_CARTESIAN_AXIS_TICK_COLOR:
		axis->tick.color = color;
	break;
	case LP_CARTESIAN_AXIS_VALUE_COLOR:
		axis->value.color = color;
	break;
	case LP_CARTESIAN_AXIS_GRID_COLOR:
		axis->grid.color = color;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_enum(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, lp_enum arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_SCALE_SELECT:
		axis->div.scale = arg0;
	break;
	case LP_CARTESIAN_AXIS_PREFIX_SELECT:
		axis->prefix.select = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_pointer(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, void *arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_NAME_FONT:
		REPLACE_FONT(axis->name.font, arg0);
	break;
	case LP_CARTESIAN_AXIS_VALUE_FONT:
		REPLACE_FONT(axis->value.font, arg0);
	break;
	case LP_CARTESIAN_AXIS_PREFIX_METRIC:
		REPLACE_OBJ(axis->prefix.metric, arg0, metric_prefix);
	break;
	case LP_CARTESIAN_AXIS_PREFIX_BINARY:
		REPLACE_OBJ(axis->prefix.binary, arg0, binary_prefix);
	break;
	case LP_CARTESIAN_AXIS_NAME_TEXT:
		REPLACE_STR(axis->name.value, arg0);
	break;
	case LP_CARTESIAN_AXIS_VALUE_APPEND:
		REPLACE_STR(axis->value.append, arg0);
	break;
	case LP_CARTESIAN_AXIS_VALUE_PREPEND:
		REPLACE_STR(axis->value.prepend, arg0);
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_uint(lp_cartesian_axis *axis,
			lp_cartesian_axis_part part, unsigned arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	switch (part) {
	case LP_CARTESIAN_AXIS_DIV_COUNT_MAX:
		axis->div.count = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_axis_values(lp_cartesian_axis *axis,
	uint8_t **names, float *positions, size_t count)
{
	LOG_ASSERT_RET_ON_NULL_ARG(axis);

	LOG_DEBUG("lp_cartesian_axis_values is not implemented yet!");

	axis->value.user.names = names;
	axis->value.user.positions = positions;
	axis->value.user.count = count;
}
