/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESIAN_CURVE_H
#define LIBREPLOT_CARTESIAN_CURVE_H

#include "../common.h"

__BEGIN_DECLS

typedef struct lp_cartesian_curve lp_cartesian_curve;

LP_API lp_cartesian_curve *lp_cartesian_curve_gen(void);
LP_API void lp_cartesian_curve_ref(lp_cartesian_curve *cartesian_curve);
LP_API void lp_cartesian_curve_del(lp_cartesian_curve *cartesian_curve);

/**
 * Usecase 1: you have data in an array:
 *      vbo = 0
 *      stride = space between samples (in bytes)
 *      pointer = pointer to array
 *
 * Usecase 2: you have VBO (Vertex buffer object) that contain axis data:
 *     vbo = <vbo>
 *     stride = space between samples. (in bytes).
 *     pointer = NULL + <first-sample-offset-in-bytes>
 *
 * Psuedo implementation:
 *  If you understand OpenGL code, this will make sense to you. (else skip)
 *  note: <axis-data-attr-id> is internally generated
 *
 *  glBindBuffer(GL_ARRAY_BUFFER, <vbo>);
 *  glEnableVertexAttribArray(<axis-data-attr-id>);
 *  glVertexAttribPointer(<axis-data-attr-id>, 1, <type>, false, <stride>, <pointer>);
 *
 * [normalize has no use (atleast unable to think) while showing data, so set to false]
 *
 * you can read the above psuedo OpenGL code (or the actual code in curve.c)
 *   and glBindBuffer, glEnableVertexAttribArray, glVertexAttribPointer
 *
 * side note:
 *  each lp_curve_axis have its own axis.count why?
 *  so, that application can keep two different array.
 *  the final program will find the minimum and use that for plotting.
 *
 * generally:
 *  vbo = 0 (data is not in VBO)
 *  type = GL_FLOAT   (IEEE32 bit data - OpenGL ES only support float)
 *  stride = 0 (Tightly packed OR number of bytes after samples)
 *  pointer = non-NULL (pointer to data)
 */

enum lp_cartesian_curve_data_axis {
	LP_CARTESIAN_CURVE_X = 0,
	LP_CARTESIAN_CURVE_Y = 1
};

typedef enum lp_cartesian_curve_data_axis lp_cartesian_curve_data_axis;

LP_API void lp_cartesian_curve_data(lp_cartesian_curve *cartesian_curve,
				lp_cartesian_curve_data_axis axis,
				GLint vbo, GLenum type, GLsizei stride,
				const GLvoid *pointer, GLsizei count);

enum lp_cartesian_curve_part {
	/* bool */
	LP_CARTESIAN_CURVE_FILL_SHOW = 0,
	LP_CARTESIAN_CURVE_LINE_SHOW = 1,
	LP_CARTESIAN_CURVE_STICK_SHOW = 2,
	LP_CARTESIAN_CURVE_MARKER_SHOW = 3,
	LP_CARTESIAN_CURVE_DOT_SHOW = 4,

	/* float */
	LP_CARTESIAN_CURVE_LINE_WIDTH = 10,
	LP_CARTESIAN_CURVE_STICK_WIDTH = 11,
	LP_CARTESIAN_CURVE_TRANSLATE_X = 12,
	LP_CARTESIAN_CURVE_TRANSLATE_Y = 13,
	LP_CARTESIAN_CURVE_DOT_WIDTH = 14,

	/* color */
	LP_CARTESIAN_CURVE_COLOR = 20,
	LP_CARTESIAN_CURVE_LINE_COLOR = 21,
	LP_CARTESIAN_CURVE_MARKER_COLOR = 22,
	LP_CARTESIAN_CURVE_FILL_COLOR = 23,
	LP_CARTESIAN_CURVE_DOT_COLOR = 24,

	/* pointer: lp_texture */
	LP_CARTESIAN_CURVE_MARKER_TEXTURE = 30,

	/* pointer: string */
	LP_CARTESIAN_CURVE_NAME_VALUE = 40,
};

typedef enum lp_cartesian_curve_part lp_cartesian_curve_part;

LP_API void lp_cartesian_curve_bool(lp_cartesian_curve *cartesian_curve,
			lp_cartesian_curve_part part, bool arg0);

LP_API void lp_cartesian_curve_float(lp_cartesian_curve *cartesian_curve,
			lp_cartesian_curve_part part, float arg0);

LP_API void lp_cartesian_curve_4float(lp_cartesian_curve *cartesian_curve,
	lp_cartesian_curve_part part,
	float arg0, float arg1, float arg2, float arg3);

LP_API void lp_cartesian_curve_pointer(lp_cartesian_curve *cartesian_curve,
			lp_cartesian_curve_part part, void *arg0);

LP_API void lp_cartesian_curve_renew(lp_cartesian_curve *cartesian_curve, unsigned tag);

__END_DECLS

#endif
