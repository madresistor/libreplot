/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESIAN_CURVE_PRIVATE_H
#define LIBREPLOT_CARTESIAN_CURVE_PRIVATE_H

#include "../common.h"
#include "curve.h"
#include "../extra/obj-manager-private.h"
#include "../extra/texture-private.h"

__BEGIN_DECLS

struct lp_cartesian_curve {
	lp_obj_manager obj_manager;

	uint8_t *name;

	struct {
		lp_vec2f translate;
	} ui;

	struct lp_cartesian_curve_data {
		GLint vbo;
		GLenum type;
		GLsizei stride;
		const GLvoid *pointer;
		GLsizei count;
	} x, y;

	struct {
		bool show;
		float width;
		lp_color4f color;
	} line;

	struct {
		bool show;
		lp_texture *texture;
		lp_color4f color; /* default: (1,1,1,1) */
	} marker;

	struct {
		bool show;
		float width;
		lp_color4f color;
	} stick;

	struct {
		bool show;
		lp_color4f color;
	} fill;

	struct {
		bool show;
		lp_color4f color;
		float size;
	} dot;
};

__END_DECLS

#endif
