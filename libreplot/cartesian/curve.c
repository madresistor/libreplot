/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "curve-private.h"

static const lp_cartesian_curve DEFAULT = {
	.name = NULL,

	.ui = {
		.translate = LP_TRANSLATE_NONE
	},

	.x = {
		.vbo = 0,
		.type = 0,
		.stride = 0,
		.pointer = NULL,
		.count = 0
	},

	.y = {
		.vbo = 0,
		.type = 0,
		.stride = 0,
		.pointer = NULL,
		.count = 0
	},

	.line = {
		.show = true,
		.width = 1,
		.color = LP_COLOR_BLACK
	},

	.marker = {
		.show = false,
		.texture = NULL,
		.color = LP_COLOR_RED,
	},

	.stick = {
		.show = false,
		.width = 1,
		.color  = LP_COLOR_BLACK
	},

	.fill = {
		.show = false,
		.color = LP_COLOR_BLACK
	},

	.dot = {
		.show = false,
		.size = 1,
		.color = LP_COLOR_BLACK
	}
};

lp_cartesian_curve *lp_cartesian_curve_gen(void)
{
	lp_cartesian_curve *curve = malloc(sizeof(*curve));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(curve, NULL);
	*curve = DEFAULT;
	lp_obj_manager_init(&curve->obj_manager);
	return curve;
}

OBJ_REF_FUNC(cartesian_curve)

void lp_cartesian_curve_del(lp_cartesian_curve *curve)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	if (lp_obj_manager_fini(&curve->obj_manager)) {
		DEL_OBJ(curve->marker.texture, lp_texture_del);
		free(curve);
	}
}

void lp_cartesian_curve_data(lp_cartesian_curve *curve,
	lp_cartesian_curve_data_axis axis,
	GLint vbo, GLenum type, GLsizei stride, const GLvoid *pointer,
	GLsizei count)
{
	struct lp_cartesian_curve_data *data;

	LOG_ASSERT_RET_ON_NULL_ARG(curve);
	if (axis == LP_CARTESIAN_CURVE_X) {
		data = &curve->x;
	} else if (axis == LP_CARTESIAN_CURVE_Y) {
		data = &curve->y;
	} else {
		LOG_WARN("unknown axis: %i", axis);
		return;
	}

	data->vbo = vbo;
	data->type = type;
	data->stride = stride;
	data->pointer = pointer;
	data->count = count;
}

void lp_cartesian_curve_bool(lp_cartesian_curve *curve,
			lp_cartesian_curve_part part, bool arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	switch (part) {
	case LP_CARTESIAN_CURVE_FILL_SHOW:
		curve->fill.show = arg0;
	break;
	case LP_CARTESIAN_CURVE_LINE_SHOW:
		curve->line.show = arg0;
	break;
	case LP_CARTESIAN_CURVE_STICK_SHOW:
		curve->stick.show = arg0;
	break;
	case LP_CARTESIAN_CURVE_MARKER_SHOW:
		curve->marker.show = arg0;
	break;
	case LP_CARTESIAN_CURVE_DOT_SHOW:
		curve->dot.show = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_curve_float(lp_cartesian_curve *curve,
			lp_cartesian_curve_part part, float arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	switch (part) {
	case LP_CARTESIAN_CURVE_LINE_WIDTH:
		curve->line.width = arg0;
	break;
	case LP_CARTESIAN_CURVE_STICK_WIDTH:
		curve->stick.width = arg0;
	break;
	case LP_CARTESIAN_CURVE_TRANSLATE_X:
		curve->ui.translate.x = arg0;
	break;
	case LP_CARTESIAN_CURVE_TRANSLATE_Y:
		curve->ui.translate.y = arg0;
	break;
	case LP_CARTESIAN_CURVE_DOT_WIDTH:
		curve->dot.size = arg0;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_curve_4float(lp_cartesian_curve *curve,
	lp_cartesian_curve_part part,
	float arg0, float arg1, float arg2, float arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	lp_color4f color = {arg0, arg1, arg2, arg3};

	switch (part) {
	case LP_CARTESIAN_CURVE_COLOR:
		curve->line.color =
		curve->marker.color =
		curve->fill.color = color;
	break;
	case LP_CARTESIAN_CURVE_LINE_COLOR:
		curve->line.color = color;
	break;
	case LP_CARTESIAN_CURVE_MARKER_COLOR:
		curve->marker.color = color;
	break;
	case LP_CARTESIAN_CURVE_FILL_COLOR:
		curve->fill.color = color;
	break;
	case LP_CARTESIAN_CURVE_DOT_COLOR:
		curve->dot.color = color;
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_curve_pointer(lp_cartesian_curve *curve,
			lp_cartesian_curve_part part, void *arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	switch (part) {
	case LP_CARTESIAN_CURVE_MARKER_TEXTURE:
		REPLACE_TEXTURE(curve->marker.texture, arg0);
	break;
	case LP_CARTESIAN_CURVE_NAME_VALUE:
		REPLACE_STR(curve->name, arg0);
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

/**
 * Reset the data if it is formed from VBO
 */
static void reset_data_if_vbo(struct lp_cartesian_curve_data *data)
{
	if (data->vbo != 0) {
		data->vbo = 0;
		data->pointer = NULL;
		data->stride = 0;
		data->count = 0;
	}
}

void lp_cartesian_curve_renew(lp_cartesian_curve *curve, unsigned tag)
{
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	if (lp_obj_manager_renew(&curve->obj_manager, tag)) {
		RENEW_TEXTURE(curve->marker.texture, tag);

		/* invalidate X, Y VBO since context is lost */
		reset_data_if_vbo(&curve->x);
		reset_data_if_vbo(&curve->y);
	}
}
