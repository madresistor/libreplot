/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "cartesian-private.h"
#include "axis-private.h"
#include "../extra/font-private.h"
#include "../extra/nice-ticks-private.h"
#include "../extra/metric-prefix-private.h"
#include "curve-private.h"

/*
 *    +-----------------------+
 *    |                       |
 * ^ 6|                       |
 * A 5|                       |
 * X 4|                       |
 * I 3|                       |
 * S 2|                       |
 * V 1|                       |
 *    +-----------------------+
 *     1  2  3  4  5  6
 *       < A X I S >
 */

static const lp_cartesian DEFAULT = {
	.dpi = {0, 0},
	.surface = {0, 0, 0, 0},

	.reuse = {
		.hb_buffer = NULL,
		.marker = NULL
	},

	.axis = {
		.bottom = NULL,
		.left = NULL,
		.top = NULL,
		.right = NULL
	},

	.program = {
		.generic = NULL,
		.circle = NULL,
		.text = NULL,
		.curve = NULL
	},
};

lp_cartesian *lp_cartesian_gen(void)
{
	lp_cartesian *cartesian = malloc(sizeof(*cartesian));
	LOG_ASSERT_RET_ON_ALLOC_FAIL(cartesian, NULL);
	*cartesian = DEFAULT;

	cartesian->program.generic = lp_program_generic_gen(NULL);
	cartesian->program.circle = lp_program_circle_gen(NULL);
	cartesian->program.text = lp_program_text_gen(NULL);
	cartesian->program.curve = lp_program_curve_gen(NULL);

	lp_obj_manager_init(&cartesian->obj_manager);

	return cartesian;
}

OBJ_REF_FUNC(cartesian)

void lp_cartesian_del(lp_cartesian *cartesian)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	if (lp_obj_manager_fini(&cartesian->obj_manager)) {
		lp_program_generic_del(cartesian->program.generic);
		lp_program_circle_del(cartesian->program.circle);
		lp_program_text_del(cartesian->program.text);

		DEL_OBJ(cartesian->reuse.hb_buffer, hb_buffer_destroy);
		DEL_OBJ(cartesian->reuse.marker, lp_texture_del);

		free(cartesian);
	}
}

void lp_cartesian_4uint(lp_cartesian *cartesian,
	lp_cartesian_part part,
	unsigned arg0, unsigned arg1, unsigned arg2, unsigned arg3)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	switch (part) {
	case LP_CARTESIAN_SURFACE:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg2 > 0);
		LOG_ASSERT_RET_ON_FAIL_ARG(arg3 > 0);
		cartesian->surface = (lp_surface){arg0, arg1, arg2, arg3};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

void lp_cartesian_2float(lp_cartesian *cartesian,
	lp_cartesian_part part, float arg0, float arg1)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	switch (part) {
	case LP_CARTESIAN_DPI:
		LOG_ASSERT_RET_ON_FAIL_ARG(arg0 > 0);
		LOG_ASSERT_RET_ON_FAIL_ARG(arg1 > 0);
		cartesian->dpi = (lp_vec2f){arg0, arg1};
	break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

LP_API void lp_cartesian_pointer(lp_cartesian *cartesian,
	lp_cartesian_part part, void *arg0)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	switch (part) {
	case LP_CARTESIAN_AXIS_AT_BOTTOM:
	case LP_CARTESIAN_AXIS_AT_LEFT:
	case LP_CARTESIAN_AXIS_AT_TOP:
	case LP_CARTESIAN_AXIS_AT_RIGHT: {
		lp_cartesian_axis **axis_ptr;
		axis_ptr = &cartesian->axis_list[part - LP_CARTESIAN_AXIS_AT_BOTTOM];
		REPLACE_OBJ((*axis_ptr), arg0, cartesian_axis);
	} break;
	default:
		SWITCH_ENUM_INCORRECT(part);
	}
}

/**
 * Get a temporary (reusable) harfbuzz buffer
 * @param cartesian Cartesian object
 * @return harbuzz buffer object
 */
static hb_buffer_t *get_tmp_hb_buffer(lp_cartesian *cartesian)
{
	if (cartesian->reuse.hb_buffer == NULL) {
		cartesian->reuse.hb_buffer = hb_buffer_create();
	}

	return cartesian->reuse.hb_buffer;
}

/**
 * Calculate the box dimention of the string
 * @param cartesian Cartesian object
 * @param axis Axis
 * @param str String
 * @param font Font (if null, use default)
 * @param rotate Rotation angle
 * @param height Height of text
 * @param [out] result Result values
 */
static void calc_string_box_dimen(lp_cartesian *cartesian,
	uint8_t *str, lp_font *font, float rotate, float height, lp_vec2f *dimen)
{
	float sx = 2.0 / cartesian->surface.width;
	float sy = 2.0 / cartesian->surface.height;
	float xdpi = cartesian->dpi.x;
	float ydpi = cartesian->dpi.y;
	float w = 0, h = 0;

	if (font == NULL) {
		font = lp_font_gen();
	}

	if (rotate) {
		SWAP_VALUE(sx, sy, float);
		SWAP_VALUE(xdpi, ydpi, float);
	}

	if (FT_Set_Char_Size(font->ft_face, 0, height * 64., xdpi, ydpi)) {
		LOG_WARN("FT_Set_Char_Size failed");
	}

	hb_buffer_t *hb_buffer = get_tmp_hb_buffer(cartesian);
	hb_buffer_reset(hb_buffer);
	hb_buffer_add_utf8(hb_buffer, (const char *) str, -1, 0, -1);
	hb_buffer_guess_segment_properties(hb_buffer);
	hb_shape(font->hb_font, hb_buffer, NULL, 0);

	unsigned int i, len = hb_buffer_get_length(hb_buffer);
	hb_glyph_info_t *info = hb_buffer_get_glyph_infos(hb_buffer, NULL);
	FT_GlyphSlot g = font->ft_face->glyph;

	for (i = 0; i < len; i++) {
		if (FT_Load_Glyph(font->ft_face, info[i].codepoint, FT_LOAD_RENDER)) {
			LOG_WARN("FT_Load_Glyph failed for code point %i", info[i].codepoint);
			continue;
		}

		w += (g->advance.x / 64.) * sx;

		float ch = g->bitmap.rows * sy;
		h = MAX(ch, h);
	}

	if (rotate) {
		SWAP_VALUE(w, h, float);
	}

	dimen->x = w;
	dimen->y = h;
}

/* http://stackoverflow.com/a/22149144/1500988 */
/* assuming value will be finite and non-zero */
static int base10_expontent(double value)
{
	double exp = log10(fabs(value));

	/* use floorf() rather than `(int) exp`.
	 * This does the proper rounding when `exp` is + or -
	 */
	return (int) floor(exp);
}

static void build_axis_value(lp_cartesian *cartesian,
	lp_cartesian_axis *axis, double value, uint8_t *buf, unsigned buf_len,
	size_t i)
{
	const uint8_t *EMPTY = (const uint8_t *) "";
	const uint8_t *prepend = axis->value.prepend;
	const uint8_t *append = axis->value.append;
	const uint8_t *prefix = EMPTY;
	uint8_t prefix_build[10];

	/* error arises due to calculation.
	 *  so, we set the comparision limit of the minumum representatble value.
	 *  (that is increment value) */
	float error_cutoff = cartesian->tmp.axis[i].nice_ticks.output.inc / 10;

	if (prepend == NULL) {
		prepend = EMPTY;
	}

	if (append == NULL) {
		append = EMPTY;
	}

	/* anything below error cutoff can be considered 0
	 *  (atleast on text representation) */
	if (fabs(value) < error_cutoff) {
		value = 0;
		goto done;
	}

	switch (axis->prefix.select) {
	case LP_CARTESIAN_AXIS_PREFIX_SELECT_NONE: {
		int exp = base10_expontent(value);
		if (abs(exp) > 2) {
			value /= powf(10, exp);
			snprintf((char *) prefix_build, sizeof prefix_build, "e%i", exp);
			prefix = prefix_build;
		}
	} break;
	case LP_CARTESIAN_AXIS_PREFIX_SELECT_METRIC: {
		const lp_metric_prefix_item *item = NULL;
		lp_metric_prefix *metric = axis->prefix.metric;
		if (metric == NULL) {
			metric = lp_metric_prefix_commonly_used();
		}

		if (!(metric->len > 0)) {
			LOG_WARN("metric should contain items");
			break;
		}

		int exp = base10_expontent(value);

		/* search for match */
		size_t j;
		for (j = 0; j < metric->len; j++) {
			item = metric->list[j];

			if ((exp - 3) < item->power) {
				break;
			}
		}

		value /= powf(10, exp);
		value *= powf(10, exp - item->power);
		prefix = item->prefix;
	} break;
	case LP_CARTESIAN_AXIS_PREFIX_SELECT_BINARY:
		LOG_DEBUG("binary prefix not implemented");
	break;
	}

	done:
	snprintf((char *) buf, buf_len, "%s%g%s%s", prepend, value, prefix, append);
}

/**
 * Draw the axis baseline.
 * baseline: the line that form the boundary of the data
 */
static void draw_axis_baseline(lp_cartesian *cartesian,
	lp_cartesian_axis *axis, unsigned i)
{
	float top = 1 - cartesian->tmp.axis[LP_CARTESIAN_TOP].margin;
	float bottom = -1 + cartesian->tmp.axis[LP_CARTESIAN_BOTTOM].margin;
	float right = 1 - cartesian->tmp.axis[LP_CARTESIAN_RIGHT].margin;
	float left = -1 + cartesian->tmp.axis[LP_CARTESIAN_LEFT].margin;

	lp_vec2f top_left = {left, top};
	lp_vec2f top_right = {right, top};
	lp_vec2f bottom_left = {left, bottom};
	lp_vec2f bottom_right = {right, bottom};

	lp_vec2f coords[4][2] = {
		{bottom_left, bottom_right},
		{bottom_left, top_left},
		{top_left, top_right},
		{top_right, bottom_right}
	};

	lp_vec2f scale = LP_SCALE_NONE;
	lp_vec2f translate = LP_TRANSLATE_NONE;

	lp_program_generic_draw_line_primitive(cartesian->program.generic,
				coords[i], 2, &axis->baseline.color, axis->baseline.width,
				&scale, &translate);
}

/**
 * Draw the axis name.
 */
static void draw_axis_name(lp_cartesian *cartesian, lp_cartesian_axis *axis,
	unsigned i)
{
	float hcenter = (cartesian->tmp.axis[LP_CARTESIAN_LEFT].margin -
		cartesian->tmp.axis[LP_CARTESIAN_RIGHT].margin) / 2;
	float vcenter = (cartesian->tmp.axis[LP_CARTESIAN_BOTTOM].margin -
		cartesian->tmp.axis[LP_CARTESIAN_TOP].margin) / 2;

	lp_vec2f coords[4] = {
		{hcenter, -1},
		{-1, vcenter},
		{hcenter, 1},
		{1, vcenter}
	};

	static const lp_align align[4] = {
		LP_ALIGN_TOP | LP_ALIGN_HCENTER,
		LP_ALIGN_VCENTER | LP_ALIGN_RIGHT,
		LP_ALIGN_BOTTOM | LP_ALIGN_HCENTER,
		LP_ALIGN_VCENTER | LP_ALIGN_LEFT
	};

	static const float rotate[4] = {0, M_PI_2, 0, M_PI_2};

	/* prepare the content */
	hb_buffer_t *hb_buffer = get_tmp_hb_buffer(cartesian);
	hb_buffer_reset(hb_buffer);
	hb_buffer_add_utf8(hb_buffer, (const char *) axis->name.value, -1, 0, -1);
	hb_buffer_guess_segment_properties(hb_buffer);

	lp_vec2f translate = LP_TRANSLATE_NONE;

	lp_program_text_draw_primitive(cartesian->program.text,
		hb_buffer, &coords[i], &cartesian->surface, &cartesian->dpi,
		axis->name.font, &axis->name.color, axis->name.height,
		align[i], rotate[i], &translate);
}

static void draw_axis_tick_value_grid(lp_cartesian *cartesian,
		lp_cartesian_axis *axis, unsigned i)
{
	float top = 1 - cartesian->tmp.axis[LP_CARTESIAN_TOP].margin;
	float bottom = -1 + cartesian->tmp.axis[LP_CARTESIAN_BOTTOM].margin;
	float right = 1 - cartesian->tmp.axis[LP_CARTESIAN_RIGHT].margin;
	float left = -1 + cartesian->tmp.axis[LP_CARTESIAN_LEFT].margin;
	float scale, translate;

	if (i & 0x1) {
		scale = cartesian->tmp.scale.y;
		translate = cartesian->tmp.translate.y;
	} else {
		scale = cartesian->tmp.scale.x;
		translate = cartesian->tmp.translate.x;
	}

	lp_nice_ticks_input *input = &cartesian->tmp.axis[i].nice_ticks.input;
	lp_nice_ticks_output *output = &cartesian->tmp.axis[i].nice_ticks.output;

	float surface_size = (i & 0x1) ?
		cartesian->surface.width :
		cartesian->surface.height;

	float tick_length = LP_PX_TO_OPENGL(axis->tick.length, surface_size);
	if (i < 2) {
		tick_length *= -1;
	}

	float constant_pos_max_side[4] = {top, right, bottom, left};
	float constant_pos_min_side[4] = {bottom, left, top, right};
	float baseline_constant_pos = constant_pos_min_side[i];
	float tick_constant_pos = baseline_constant_pos + tick_length;
	float grid_constant_pos = constant_pos_max_side[i];

	static const lp_align value_align[4] = {
		LP_ALIGN_BOTTOM | LP_ALIGN_HCENTER,
		LP_ALIGN_VCENTER | LP_ALIGN_LEFT,
		LP_ALIGN_TOP | LP_ALIGN_HCENTER,
		LP_ALIGN_VCENTER | LP_ALIGN_RIGHT
	};

	static lp_vec2f scale_none = LP_SCALE_NONE;
	static lp_vec2f translate_none = LP_TRANSLATE_NONE;

	double v;
	for (v = output->min; v < output->max; v += output->inc) {
		if (v <= input->min) {
			continue;
		}

		if (v >= input->max) {
			break;
		}

		float vv = (v * scale) + translate;

		/* shared values.
		 *  coordinate is shared by value, tick and grid */
		lp_vec2f coords[2];
		if (i & 0x1) {
			coords[0].y = coords[1].y = vv;
			coords[0].x = baseline_constant_pos;
		} else {
			coords[0].x = coords[1].x = vv;
			coords[0].y = baseline_constant_pos;
		}

		/* value and tick share 1 coordinate.
		 *  tick require coord 0 and 1
		 * and value require coord 1
		 */
		if (axis->tick.show || axis->value.show) {
			if (i & 0x1) {
				coords[1].x = tick_constant_pos;
			} else {
				coords[1].y = tick_constant_pos;
			}
		}

		if (axis->tick.show) {
			lp_program_generic_draw_line_primitive(cartesian->program.generic,
					coords, 2, &axis->tick.color, axis->tick.width,
					&scale_none, &translate_none);
		}

		if (axis->value.show) {
			/* prepare the content */
			uint8_t buf[50];

			build_axis_value(cartesian, axis, v, buf, sizeof buf, i);

			hb_buffer_t *hb_buffer = get_tmp_hb_buffer(cartesian);
			hb_buffer_reset(hb_buffer);
			hb_buffer_add_utf8(hb_buffer, (const char *) buf, -1, 0, -1);
			hb_buffer_guess_segment_properties(hb_buffer);

			lp_program_text_draw_primitive(cartesian->program.text,
				hb_buffer, &coords[1], &cartesian->surface, &cartesian->dpi,
				axis->value.font, &axis->value.color, axis->value.height,
				value_align[i], 0, &translate_none);
		}

		if (axis->grid.show) {
			/* reuse the same coords for grid */
			if (i & 0x1) {
				coords[1].x = grid_constant_pos;
			} else {
				coords[1].y = grid_constant_pos;
			}

			lp_program_generic_draw_line_primitive(cartesian->program.generic,
					coords, 2, &axis->grid.color, axis->grid.width,
					&scale_none, &translate_none);
		}
	}
}

/**
 * Draw all axis
 * @param cartesian Cartesian object
 */
static void draw_axis(lp_cartesian *cartesian)
{
	unsigned i;

	for (i = 0; i < 4; i++) {
		lp_cartesian_axis *axis = cartesian->axis_list[i];

		/* only process if not NULL */
		if (axis == NULL) {
			continue;
		}

		if (axis->name.show) {
			draw_axis_name(cartesian, axis, i);
		}

		if (axis->tick.show || axis->grid.show) {
			draw_axis_tick_value_grid(cartesian, axis, i);
		}

		if (axis->baseline.show) {
			draw_axis_baseline(cartesian, axis, i);
		}
	}
}

/**
 * Calculate the margin.
 *   after removing the margin from area, the remaining space is used for curve.
 * @param cartesian Cartesian object
 * @param rudimentary_mode if true, no dynamic size is calculated.
 *    only basic assumptions are included instead.
 */
static void calc_margin(lp_cartesian *cartesian, bool rudimentary_mode)
{
	size_t i;
	static const float rotate[4] = {0, M_PI_2, 0, M_PI_2};
	uint8_t buf[20];

	for (i = 0; i < 4; i++) {
		lp_cartesian_axis *axis = cartesian->axis_list[i];
		float calc = 0;

		/* we have axis to calculate for? */
		if (axis == NULL) {
			goto done;
		}

		float surface_size = (i & 0x1) ?
				cartesian->surface.width :
				cartesian->surface.height;

		/* add height of name to margin */
		if (axis->name.show) {
			if (rudimentary_mode) {
				calc += LP_PX_TO_OPENGL(axis->name.height, surface_size);
			} else {
				lp_vec2f dimen;
				calc_string_box_dimen(cartesian, axis->name.value,
					axis->name.font, rotate[i], axis->name.height, &dimen);
				calc += (i & 0x1) ? dimen.x : dimen.y;
			}
		}

		if (axis->value.show) {
			if (rudimentary_mode) {
				float atleast = LP_PX_TO_OPENGL(axis->value.height, surface_size);
				calc += atleast;
				cartesian->tmp.axis[i].auto_spacing_calc = atleast;
			} else {
				/* in rudimentary_mode=false,
				 *  ticks text will be used to calculate the text */
				lp_nice_ticks_input *input = &cartesian->tmp.axis[i].nice_ticks.input;
				lp_nice_ticks_output *output = &cartesian->tmp.axis[i].nice_ticks.output;
				double v;
				lp_vec2f max = {0, 0};

				for (v = output->min; v < output->max; v += output->inc) {
					if (v <= input->min) {
						continue;
					}

					if (v >= input->max) {
						break;
					}

					build_axis_value(cartesian, axis, v, buf, sizeof(buf), i);

					lp_vec2f dimen;
					calc_string_box_dimen(cartesian, buf, axis->value.font,
						0, axis->value.height, &dimen);
					max.x = MAX(dimen.x, max.x);
					max.y = MAX(dimen.y, max.y);
				}

				calc += (i & 0x1) ? max.x : max.y;

				/* this is used in future to
				 *  prevent overlapping of axis values.
				 * Also, there should be gap between text. (currently 10%)*/
				float ss, auto_spacing, dpi;
				if (i & 0x1) {
					ss = cartesian->surface.height;
					dpi = cartesian->dpi.y;
					auto_spacing = max.y;
				} else {
					ss = cartesian->surface.width;
					dpi = cartesian->dpi.x;
					auto_spacing = max.x;
				}
				auto_spacing *= 1.6; /* 60% extra should be empty */
				auto_spacing = LP_OPENGL_TO_PX(auto_spacing, ss);
				auto_spacing = LP_PX_TO_MM(auto_spacing, dpi);
				cartesian->tmp.axis[i].auto_spacing_calc = auto_spacing;
			}
		}

		/* if baseline is being shown,
		 *  add the width/2 to margin */
		if (axis->baseline.show) {
			calc += LP_PX_TO_OPENGL(axis->baseline.width, surface_size) / 2;
		}

		/* if ticks are being shown, add their length too. */
		if (axis->tick.show) {
			calc += LP_PX_TO_OPENGL(axis->tick.length, surface_size);
		}

		/* store the value */
		done:
		cartesian->tmp.axis[i].margin = calc;
	}
}

/**
 * Get the number of ticks to be drawn.
 * If the value is not provided by application, value is calculated.
 * @param cartesian Cartesian
 * @param axis Axis
 * @param i Index
 * @param a non-zero value
 * @param rudimentary_mode if true, margin is not included
 *   (uscase: margin not calculate yet!)
 */
static unsigned axis_ticks_count(lp_cartesian *cartesian,
		lp_cartesian_axis *axis, size_t i, bool rudimentary_mode)
{
	float dpi;
	float surface;
	unsigned margin = 0;
	unsigned value = axis->div.count;

	if (value) {
		/* provided by application code */
		goto done;
	}

	if (i & 0x1) {
		dpi = cartesian->dpi.y;
		surface = cartesian->surface.height;
		if (!rudimentary_mode) {
			margin += cartesian->tmp.axis[LP_CARTESIAN_BOTTOM].margin;
			margin += cartesian->tmp.axis[LP_CARTESIAN_TOP].margin;
		}
	} else {
		dpi = cartesian->dpi.x;
		surface = cartesian->surface.width;
		if (!rudimentary_mode) {
			margin += cartesian->tmp.axis[LP_CARTESIAN_LEFT].margin;
			margin += cartesian->tmp.axis[LP_CARTESIAN_RIGHT].margin;
		}
	}

	/* substract the margin area */
	surface -= LP_OPENGL_TO_PX(margin, surface);

	/* surface to small? */
	if (surface < 10) {
		value = 5;
		goto done;
	}

	/* 1 tick per [auto_spacing_hint] */
	float available = LP_INCH_TO_MM(surface / dpi);
	float auto_spacing_user = axis->div.auto_spacing_hint; /* provided by user */
	float auto_spacing_calc = cartesian->tmp.axis[i].auto_spacing_calc; /* calculated */
	float auto_spacing = MAX(auto_spacing_user, auto_spacing_calc);
	value = floor(available / auto_spacing);

	done:
	return MAX(value, 5);
}

/**
 * Calculate ticks of all the axis.
 * @param cartesian Cartesian
 * @param rudimentary_mode ( passed to axis_ticks_count() )
 */
static void calc_nice_ticks(lp_cartesian *cartesian, bool rudimentary_mode)
{
	unsigned i;

	for (i = 0; i < 4; i++) {
		/* do not calculate for axis (top, right) whose
		 *  first consider counter part axis (bottom, left)
		 *  are not NULL (ie provided)
		 */
		if (i >= 2 && cartesian->axis_list[i - 2] != NULL) {
			continue;
		}

		lp_cartesian_axis *axis = cartesian->axis_list[i];

		if (axis == NULL) {
			continue;
		}

		lp_nice_ticks_input *input = &cartesian->tmp.axis[i].nice_ticks.input;
		lp_nice_ticks_output *output = &cartesian->tmp.axis[i].nice_ticks.output;

		input->min = axis->value.min;
		input->max = axis->value.max;
		input->ticks = axis_ticks_count(cartesian, axis, i, rudimentary_mode);

		lp_nice_ticks(input, output);
	}
}

static inline float range_conv_calc_scale(float from_min, float from_max,
	float to_min, float to_max)
{
	float tmp1 = to_max - to_min;
	float tmp2 = from_max - from_min;
	return tmp1 / tmp2;
}

static inline float range_conv_calc_translate(float from_min, float from_max,
	float to_min, float to_max)
{
	float tmp1 = (from_max * to_min) - (from_min * to_max);
	float tmp2 = from_max - from_min;
	return tmp1 / tmp2;
}

static inline void store_scale_and_translate(
	float *translate, float *scale,
	float from_min, float from_max,
	float to_min, float to_max)
{
	*translate = range_conv_calc_translate(from_min, from_max, to_min, to_max);
	*scale = range_conv_calc_scale(from_min, from_max, to_min, to_max);
}

static inline void calc_scale_and_translate_for_interaction(
	float *translate, float *scale,
	lp_cartesian_axis *axis)
{
	store_scale_and_translate(
		translate, scale,
		axis->value.min, axis->value.max,
		-1, 1);
}

static inline void calc_scale_and_translate_for_scissor(
	float *translate, float *scale,
	lp_cartesian *cartesian,
	unsigned axis_min, unsigned axis_max)
{
	float min = -1 + cartesian->tmp.axis[axis_min].margin;
	float max = 1 - cartesian->tmp.axis[axis_max].margin;

	store_scale_and_translate(
		translate, scale,
		-1, 1,
		min, max);
}

static inline void merge_translate(float *translate, float scale2,
	float translate1, float translate2)
{
	*translate = (scale2 * translate1) + translate2;
}

static inline void merge_scale(float *scale, float scale1, float scale2)
{
	*scale = scale1 * scale2;
}

static void calc_scale_and_translate(lp_cartesian *cartesian)
{
	lp_cartesian_axis *vert_axis, *horiz_axis;
	lp_vec2f scale1 = LP_SCALE_NONE, translate1 = LP_TRANSLATE_NONE;
	lp_vec2f scale2, translate2;

	if ((horiz_axis = cartesian->axis.bottom) != NULL ||
		(horiz_axis = cartesian->axis.top) != NULL) {
		calc_scale_and_translate_for_interaction(&translate1.x, &scale1.x,
			horiz_axis);
	}

	if ((vert_axis = cartesian->axis.left) != NULL ||
		(vert_axis = cartesian->axis.right) != NULL) {
		calc_scale_and_translate_for_interaction(&translate1.y, &scale1.y,
			vert_axis);
	}

	calc_scale_and_translate_for_scissor(&translate2.x, &scale2.x, cartesian,
		LP_CARTESIAN_LEFT, LP_CARTESIAN_RIGHT);

	calc_scale_and_translate_for_scissor(&translate2.y, &scale2.y, cartesian,
		LP_CARTESIAN_BOTTOM, LP_CARTESIAN_TOP);

	/* merge the two scale and translates */

	merge_translate(&cartesian->tmp.translate.x, scale2.x, translate1.x, translate2.x);
	merge_translate(&cartesian->tmp.translate.y, scale2.y, translate1.y, translate2.y);

	merge_scale(&cartesian->tmp.scale.x, scale1.x, scale2.x);
	merge_scale(&cartesian->tmp.scale.y, scale1.y, scale2.y);
}

/**
 * Meant to be called at end of lp_cartesian_draw_start()
 */
static void prepare_for_data(lp_cartesian *cartesian)
{
	GLint x, y;
	GLsizei width, height;

	lp_surface *surface = &cartesian->surface;
	float bottom = cartesian->tmp.axis[LP_CARTESIAN_BOTTOM].margin;
	float left = cartesian->tmp.axis[LP_CARTESIAN_LEFT].margin;
	float top = cartesian->tmp.axis[LP_CARTESIAN_TOP].margin;
	float right = cartesian->tmp.axis[LP_CARTESIAN_RIGHT].margin;

	x = surface->x + LP_OPENGL_TO_PX(left, surface->width);
	y = surface->y + LP_OPENGL_TO_PX(bottom, surface->height);
	width = surface->width - LP_OPENGL_TO_PX((left + right), surface->width);
	height = surface->height - LP_OPENGL_TO_PX((bottom + top), surface->height);

	LOG_GL_ERROR(glScissor(x, y, width, height));
	LOG_GL_ERROR(glEnable(GL_SCISSOR_TEST));

	/* store for future (pixel to data coord conversion) */
	cartesian->tmp.scissor_area.x = x;
	cartesian->tmp.scissor_area.y = y;
	cartesian->tmp.scissor_area.width = width;
	cartesian->tmp.scissor_area.height = height;

	lp_cartesian_axis *axis;

	/* cache Y Axis min max */
	if ((axis = cartesian->axis.bottom) != NULL ||
		(axis = cartesian->axis.top) != NULL) {
		cartesian->tmp.cache_axis.x.min = axis->value.min;
		cartesian->tmp.cache_axis.x.max = axis->value.max;
	}

	/* cache Y Axis min max */
	if ((axis = cartesian->axis.left) != NULL ||
		(axis = cartesian->axis.right) != NULL) {
		cartesian->tmp.cache_axis.y.min = axis->value.min;
		cartesian->tmp.cache_axis.y.max = axis->value.max;
	}
}

/**
 * Called before drawing any curve/item.
 * @param cartesian Cartesian
 */
void lp_cartesian_draw_start(lp_cartesian *cartesian)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	/* in calculation of vertical axis,
	 *  we need to calculate the horizontal margin first.
	 * so, rudimentary_mode=TRUE will force calculation of rudimentary calculation.
	 * later, we can perform more accurate calculation.
	 */
	calc_margin(cartesian, true);

	/* calculate nice ticks (rudimentary_mode=TRUE) */
	calc_nice_ticks(cartesian, true);

	/* now we have calculated nice ticks,
	 *  we are ready to perform better calculation */
	calc_margin(cartesian, false);

	/* recalculate ticks because auto-spacing is now calculated. */
	calc_nice_ticks(cartesian, false);

	/* calculate scale and translate that
	 *  would be required to map data to the area.
	 * The same scale and translate is also used for
	 *  ticks, values position calculation.
	 */
	calc_scale_and_translate(cartesian);

	/* start drawing the axis */
	draw_axis(cartesian);

	/* prepare the area for data */
	prepare_for_data(cartesian);
}

/**
 * Get a temporary (reusable) harfbuzz buffer
 * @param cartesian Cartesian object
 * @return harbuzz buffer object
 */
static lp_texture *get_tmp_marker(lp_cartesian *cartesian)
{
	if (cartesian->reuse.marker == NULL) {
		cartesian->reuse.marker = lp_texture_gen();
	}

	return cartesian->reuse.marker;
}


/**
 * Set data of curve
 * @param data Axis data
 * @param axis_attr GLSL Program axis attribute
 */
static void set_curve_data(struct lp_cartesian_curve_data *data, GLint axis_attr)
{
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, data->vbo));
	LOG_GL_ERROR(glEnableVertexAttribArray(axis_attr));

	/* NOTE: as per doc, if glBindBuffer(GL_ARRAY_BUFFER, <greater-than-0>),
	 *   pointer is treated as byte offset */
	LOG_GL_ERROR(glVertexAttribPointer(axis_attr, 1,
		data->type, GL_FALSE, data->stride, data->pointer));
}

void lp_cartesian_draw_curve(lp_cartesian *cartesian, lp_cartesian_curve *curve)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);
	LOG_ASSERT_RET_ON_NULL_ARG(curve);

	GLsizei count = MIN(curve->x.count, curve->y.count);
	LOG_ASSERT_RET_ON_FAIL(count > 0);

	lp_program_curve *program = cartesian->program.curve;

	lp_vec2f scale = cartesian->tmp.scale;

	lp_vec2f translate = cartesian->tmp.translate;
	translate.x += curve->ui.translate.x;
	translate.y += curve->ui.translate.y;

	LOG_GL_ERROR(glUseProgram(program->id));
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, 0));

	set_curve_data(&curve->x, program->attribute.position_x);
	set_curve_data(&curve->y, program->attribute.position_y);

	/* common */
	LOG_GL_ERROR(glUniform2fv(program->uniform.scale, 1,
			CAST_TO_FLOAT_PTR(&scale)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.translate, 1,
			CAST_TO_FLOAT_PTR(&translate)));

	if (curve->line.show) {
		/* generic */
		LOG_GL_ERROR(glLineWidth(curve->line.width));
		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1,
				CAST_TO_FLOAT_PTR(&curve->line.color)));

		LOG_GL_ERROR(glUniform1i(program->uniform.format, LP_CURVE_FORMAT_NONE));

		/* plotting by given count */
		LOG_GL_ERROR(glDrawArrays(GL_LINE_STRIP, 0, count));
	}

	if (curve->dot.show) {
		/* setup the texture */
		LOG_GL_ERROR(glUniform1i(program->uniform.format, LP_CURVE_FORMAT_NONE));
		LOG_GL_ERROR(glUniform1f(program->uniform.point_size, curve->dot.size));

		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1,
				CAST_TO_FLOAT_PTR(&curve->dot.color)));

		/* plotting by given count */
		LOG_GL_ERROR(glDrawArrays(GL_POINTS, 0, count));
	}

	if (curve->marker.show) {
		lp_texture *marker = curve->marker.texture;
		if (marker == NULL) {
			marker = get_tmp_marker(cartesian);
		}

		int format;
		switch (marker->internalFormat) {
		case GL_ALPHA:
			format = LP_CURVE_FORMAT_ALPHA;
		break;
		case GL_RGB:
			format = LP_CURVE_FORMAT_RGB;
		break;
		case GL_RGBA:
			format = LP_CURVE_FORMAT_RGBA;
		break;
		case GL_LUMINANCE:
			format = LP_CURVE_FORMAT_LUMINANCE;
		break;
		case GL_LUMINANCE_ALPHA:
			format = LP_CURVE_FORMAT_LUMINANCE_ALPHA;
		break;
		default:
			format = LP_CURVE_FORMAT_RGBA;
			LOG_WARN("internalFormat is not understood. forcing to RGBA");
		break;
		}

		/* setup the texture */
		LOG_GL_ERROR(glActiveTexture(GL_TEXTURE0));
		LOG_GL_ERROR(glBindTexture(GL_TEXTURE_2D, marker->id));
		LOG_GL_ERROR(glUniform1i(program->uniform.texture, 0));
		LOG_GL_ERROR(glUniform1i(program->uniform.format, format));
		LOG_GL_ERROR(glUniform1f(program->uniform.point_size, marker->width));

		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1,
				CAST_TO_FLOAT_PTR(&curve->marker.color)));

		/* plotting by given count */
		LOG_GL_ERROR(glDrawArrays(GL_POINTS, 0, count));
	}
}

void lp_cartesian_draw_circle(lp_cartesian *cartesian, lp_circle *circle)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);
	LOG_ASSERT_RET_ON_NULL_ARG(circle);

	lp_program_circle_draw_circle(cartesian->program.circle, circle,
		&cartesian->tmp.scale, &cartesian->tmp.translate);
}

void lp_cartesian_draw_line(lp_cartesian *cartesian, lp_line *line)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);
	LOG_ASSERT_RET_ON_NULL_ARG(line);

	lp_program_generic_draw_line(cartesian->program.generic, line,
		&cartesian->tmp.scale, &cartesian->tmp.translate);
}

void lp_cartesian_draw_text(lp_cartesian *cartesian, lp_text *text)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);
	LOG_ASSERT_RET_ON_NULL_ARG(text);

	lp_program_text_draw_text(cartesian->program.text, text,
		get_tmp_hb_buffer(cartesian), &cartesian->surface,
		&cartesian->dpi, &cartesian->tmp.translate);
}

void lp_cartesian_draw_rectangle(lp_cartesian *cartesian, lp_rectangle *rect)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);
	LOG_ASSERT_RET_ON_NULL_ARG(rect);

	lp_program_generic_draw_rectangle(cartesian->program.generic, rect,
		&cartesian->tmp.scale, &cartesian->tmp.translate);
}

void lp_cartesian_draw_end(lp_cartesian *cartesian)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	LOG_GL_ERROR(glDisable(GL_SCISSOR_TEST));
}

#define RENEW_CARTESIAN_PROGRAM(name)									\
	cartesian->program.name =											\
			lp_program_ ##name ##_gen(cartesian->program.name)

void lp_cartesian_renew(lp_cartesian *cartesian, unsigned tag)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	if (lp_obj_manager_renew(&cartesian->obj_manager, tag)) {
		RENEW_TEXTURE(cartesian->reuse.marker, tag);

		/* regenerate different program's */
		RENEW_CARTESIAN_PROGRAM(generic);
		RENEW_CARTESIAN_PROGRAM(circle);
		RENEW_CARTESIAN_PROGRAM(text);
		RENEW_CARTESIAN_PROGRAM(curve);
	}
}

static float pixel_to_data_coord(int p, GLint off, GLsizei size,
	float min, float max)
{
	if (p < off) {
		return -INFINITY;
	} else if (p > (off + size)) {
		return +INFINITY;
	}

	/* map from pixel coordinate to data coordinate */
	/* basically: y1 - y0 = m (x1 - x0)
	 *   here
	 *     x1 - x0 = size
	 *     x0 = off
	 *     y0 = min
	 */
	float m = (max - min) / size;
	return m * (p - off) + min;
}

/**
 * Convert pixel to data (plot) coord.
 * @param cartesian Cartesian
 * @param px Pixel X
 * @param py Pixel Y
 * @param cx Store coordate X
 * @param cy Store coordate Y
 * @note if @a px lies on left of plot area, @a cx = -INF
 * @note if @a px lies on right of the plot area @a cx = +INF
 * @note if @a py lies on bottom of plot area, @a cy = -INF
 * @note if @a py lies on top of the plot area @a cy = +INF
 * @warning Atleast before lp_cartesian_draw_start() and lp_cartesian_draw_end() should be called.
 * @note @a px = 0 mean left, px = GL_AREA_WIDTH means extreme right
 * @note @a py = 0 mean bottom, py = GL_AREA_HEIGHT means extreme top
 */
void lp_cartesian_pixel_to_data_coord(lp_cartesian *cartesian, int px, int py,
	float *cx, float *cy)
{
	LOG_ASSERT_RET_ON_NULL_ARG(cartesian);

	if (cx != NULL) {
		GLint off = cartesian->tmp.scissor_area.x;
		GLsizei size = cartesian->tmp.scissor_area.width;
		float min = cartesian->tmp.cache_axis.x.min;
		float max = cartesian->tmp.cache_axis.x.max;
		*cx = pixel_to_data_coord(px, off, size, min, max);
	}

	if (cy != NULL) {
		GLint off = cartesian->tmp.scissor_area.y;
		GLsizei size = cartesian->tmp.scissor_area.height;
		float min = cartesian->tmp.cache_axis.y.min;
		float max = cartesian->tmp.cache_axis.y.max;
		*cy = pixel_to_data_coord(py, off, size, min, max);
	}
}
