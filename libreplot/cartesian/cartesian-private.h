/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_CARTESAIN_PRIVATE_H
#define LIBREPLOT_CARTESAIN_PRIVATE_H

#include "../common.h"
#include "cartesian.h"
#include "../extra/obj-manager-private.h"
#include "../program/generic.h"
#include "../program/circle.h"
#include "../program/text.h"
#include "../program/curve.h"
#include "../extra/nice-ticks-private.h"
#include "../extra/texture.h"
#include <hb.h>

__BEGIN_DECLS

#define LP_CARTESIAN_BOTTOM 0
#define LP_CARTESIAN_LEFT 1
#define LP_CARTESIAN_TOP 2
#define LP_CARTESIAN_RIGHT 3

struct lp_cartesian {
	lp_obj_manager obj_manager;

	lp_vec2f dpi;
	lp_surface surface;

	/* reuable objects.
	 *  to reduce allocation and freeing.
	 */
	struct {
		hb_buffer_t *hb_buffer;
		lp_texture *marker;
	} reuse;

	struct {
		lp_program_generic *generic;
		lp_program_circle *circle;
		lp_program_text *text;
		lp_program_curve *curve;
	} program;

	union {
		/**
		 * Axis at bottom and left are recommended to be NOT NULL
		 */

		/* independent elements */
		struct {
			lp_cartesian_axis *bottom;
			lp_cartesian_axis *left;
			lp_cartesian_axis *top;
			lp_cartesian_axis *right;
		} axis;

		/* in form of array */
		lp_cartesian_axis *axis_list[4];
	};

	/* these are used temporarily but need to be stored till the library
	 *  return the control back to application code
	 */
	struct {
		struct {
			struct {
				lp_nice_ticks_input input;
				lp_nice_ticks_output output;
			} nice_ticks;

			float auto_spacing_calc;
			float margin;
		} axis[4];

		lp_vec2f scale;
		lp_vec2f translate;

		struct {
			GLint x, y;
			GLsizei width, height;
		} scissor_area;

		struct {
			struct {
				float min, max;
			} x, y;
		} cache_axis;
	} tmp;
};

__END_DECLS

#endif
