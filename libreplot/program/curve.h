/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PROGRAM_CURVE_H
#define LIBREPLOT_PROGRAM_CURVE_H

#include "common.h"

__BEGIN_DECLS

struct lp_program_curve {
	GLuint id;
	struct { GLint translate, scale, color, point_size, texture, format; } uniform;
	struct { GLint position_x, position_y; } attribute;
};

#define LP_CURVE_FORMAT_NONE 0 /* use colour, no texture */
#define LP_CURVE_FORMAT_ALPHA 1  /* texture: internalFormat = GL_ALPHA */
#define LP_CURVE_FORMAT_RGB 2 /* texture: internalFormat = GL_RGB */
#define LP_CURVE_FORMAT_RGBA 3 /* texture: internalFormat = GL_RGBA */
#define LP_CURVE_FORMAT_LUMINANCE 4 /* texture: internalFormat = GL_LUMINANCE */
#define LP_CURVE_FORMAT_LUMINANCE_ALPHA 5 /* texture: internalFormat = GL_LUMINANCE_ALPHA */

typedef struct lp_program_curve lp_program_curve;

LP_PRIV lp_program_curve *lp_program_curve_gen(lp_program_curve *program);
LP_PRIV void lp_program_curve_del(lp_program_curve *program);

__END_DECLS

#endif
