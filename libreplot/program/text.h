/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PROGRAM_TEXT_H
#define LIBREPLOT_PROGRAM_TEXT_H

#include "common.h"
#include "../extra/text.h"

#include <hb.h>

__BEGIN_DECLS

struct lp_program_text {
	GLuint id;
	struct { GLint coord; } attribute;
	struct { GLint tex, color; } uniform;
};

typedef struct lp_program_text lp_program_text;

LP_PRIV lp_program_text *lp_program_text_gen(lp_program_text *program);
LP_PRIV void lp_program_text_del(lp_program_text *program);

LP_PRIV void lp_program_text_draw_text(lp_program_text *program,
				lp_text *text, hb_buffer_t *hb_buffer, lp_surface *surface,
				lp_vec2f *dpi, lp_vec2f *translate);

LP_PRIV void lp_program_text_draw_primitive(lp_program_text *program,
				hb_buffer_t *hb_buffer, lp_vec2f *pos, lp_surface *surface,
				lp_vec2f *dpi, lp_font *font, lp_color4f *color,
				float height, lp_align align, float rotate, lp_vec2f *translate);

__END_DECLS

#endif
