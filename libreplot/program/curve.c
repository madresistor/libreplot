/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "curve.h"

static const char VERTEX_SHADER[] =
	"attribute float attribute_position_x;"
	"attribute float attribute_position_y;"
	"uniform vec2 uniform_translate;"
	"uniform vec2 uniform_scale;"
	"uniform float uniform_point_size;"
	"void main(void) {"
		"vec2 position = vec2(attribute_position_x, attribute_position_y);"
		"vec2 r = (position * uniform_scale) + uniform_translate;"
		"gl_Position = vec4(r, 0, 1);"
		"gl_PointSize = uniform_point_size;"
	"}";

static const char FRAGMENT_SHADER[] =
	"uniform vec4 uniform_color;"
	"uniform int uniform_format;"
	"uniform sampler2D uniform_texture;"

	/* internalFormat constants. GLES supported one only. */
	"const int FORMAT_NONE = 0;"
	"const int FORMAT_ALPHA = 1;"
	"const int FORMAT_RGB = 2;"
	"const int FORMAT_RGBA = 3;"
	"const int FORMAT_LUMINANCE = 4;"
	"const int FORMAT_LUMINANCE_ALPHA = 5;"

	"void main(void) {"
		/* no switch case for us, fallback to if else.
		 *  Ref: http://stackoverflow.com/a/16002538/1500988 */
		"if (uniform_format == FORMAT_NONE) {"
			"gl_FragColor = uniform_color;"

		/* Alpha only provided */
		"} else if (uniform_format == FORMAT_ALPHA) {"
			"float a = texture2D(uniform_texture, gl_PointCoord).a;"
			"gl_FragColor = a * uniform_color;"

		/* RGB only provided */
		"} else if (uniform_format == FORMAT_RGB) {"
			"vec3 rgb = texture2D(uniform_texture, gl_PointCoord).rgb;"
			"gl_FragColor = vec4(rgb, uniform_color.a);"

		/* RGBA provided */
		"} else if (uniform_format == FORMAT_RGBA) {"
			"gl_FragColor = texture2D(uniform_texture, gl_PointCoord);"

		/* RGB = constant and
		 *  FORMAT_LUMINANCE: A = 1.0
		 *  FORMAT_LUMINANCE_ALPHA: A = provided */
		"} else if (uniform_format == FORMAT_LUMINANCE"
				" || uniform_format == FORMAT_LUMINANCE_ALPHA) {"
			"vec4 rgba = texture2D(uniform_texture, gl_PointCoord).rgba;"
			"rgba *= uniform_color;"
			"gl_FragColor = rgba;"

		/* unknown! (force as RGBA) */
		"} else {"
			"gl_FragColor = texture2D(uniform_texture, gl_PointCoord);"
		"}"
	"}";

static const lp_program_curve DEFAULT = {
	.id = -1,
	.uniform = {
		.translate = -1,
		.scale = -1,
		.color = -1,
		.point_size = -1,
		.texture = -1,
		.format = -1
	},
	.attribute = {
		.position_x = -1,
		.position_y = -1
	}
};

lp_program_curve *lp_program_curve_gen(lp_program_curve *program)
{
	if (program == NULL) {
		program = malloc(sizeof(*program));
		LOG_ASSERT_RET_ON_ALLOC_FAIL(program, NULL);
	}

	*program = DEFAULT;

	LOG_DEBUG("compiling curve program");
	program->id = lp_program_create(VERTEX_SHADER, FRAGMENT_SHADER);
	program->attribute.position_x = lp_program_attribute(program->id, "attribute_position_x");
	program->attribute.position_y = lp_program_attribute(program->id, "attribute_position_y");
	program->uniform.color = lp_program_uniform(program->id, "uniform_color");
	program->uniform.scale = lp_program_uniform(program->id, "uniform_scale");
	program->uniform.translate = lp_program_uniform(program->id, "uniform_translate");
	program->uniform.point_size = lp_program_uniform(program->id, "uniform_point_size");
	program->uniform.texture = lp_program_uniform(program->id, "uniform_texture");
	program->uniform.format = lp_program_uniform(program->id, "uniform_format");

	LOG_DEBUG_INT(program->id);
	LOG_DEBUG_INT(program->attribute.position_x);
	LOG_DEBUG_INT(program->attribute.position_y);
	LOG_DEBUG_INT(program->uniform.color);
	LOG_DEBUG_INT(program->uniform.scale);
	LOG_DEBUG_INT(program->uniform.translate);
	LOG_DEBUG_INT(program->uniform.point_size);
	LOG_DEBUG_INT(program->uniform.texture);
	LOG_DEBUG_INT(program->uniform.format);

	return program;
}

void lp_program_curve_del(lp_program_curve *program)
{
	LOG_ASSERT_RET_ON_NULL_ARG(program);
	LOG_GL_ERROR(glDeleteProgram(program->id));
	free(program);
}
