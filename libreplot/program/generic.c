/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../private.h"
#include "generic.h"
#include "../extra/line-private.h"
#include "../extra/rectangle-private.h"

static const char VERTEX_SHADER[] =
	"attribute vec2 attribute_position;"
	"uniform vec2 uniform_scale;"
	"uniform vec2 uniform_translate;"
	"void main(void) {"
		"vec2 pos = (attribute_position * uniform_scale) + uniform_translate;"
		"gl_Position = vec4(pos, 0, 1);"
	"}";

static const char FRAGMENT_SHADER[] =
	"uniform vec4 uniform_color;"
	"void main(void) {"
		"gl_FragColor = uniform_color;"
	"}";

static const lp_program_generic DEFAULT = {
	.id = -1,
	.uniform = {
		.color = -1,
		.scale = -1,
		.translate = -1
	},
	.attribute = {
		.position = -1
	}
};

lp_program_generic *lp_program_generic_gen(lp_program_generic *program)
{
	if (program == NULL) {
		program = malloc(sizeof(*program));
		LOG_ASSERT_RET_ON_ALLOC_FAIL(program, NULL);
	}

	*program = DEFAULT;

	LOG_DEBUG("compiling generic program");
	program->id = lp_program_create(VERTEX_SHADER, FRAGMENT_SHADER);
	program->attribute.position = lp_program_attribute(program->id, "attribute_position");
	program->uniform.color = lp_program_uniform(program->id, "uniform_color");
	program->uniform.scale = lp_program_uniform(program->id, "uniform_scale");
	program->uniform.translate = lp_program_uniform(program->id, "uniform_translate");

	LOG_DEBUG_INT(program->id);
	LOG_DEBUG_INT(program->attribute.position);
	LOG_DEBUG_INT(program->uniform.color);
	LOG_DEBUG_INT(program->uniform.scale);
	LOG_DEBUG_INT(program->uniform.translate);

	return program;
}

void lp_program_generic_del(lp_program_generic *program)
{
	LOG_ASSERT_RET_ON_NULL_ARG(program);

	LOG_GL_ERROR(glDeleteProgram(program->id));
	free(program);
}

static inline float infinity_conv_axis(float arg, float scale, float translate)
{
	/* isinf return 0 if the value is not infinite */
	if (isinf(arg)) {
		float sign = signbit(arg) ? -1 : 1;
		arg = (sign - translate) / scale;
	}

	return arg;
}

/** if any of the value is INFINITY or -INFINITY.
 *  it is converted to such a value that can be used to draw.
 *   apply reverse of scale and translate to appear infinity
 */
static void infinity_conv(lp_vec2f *p, size_t N,
				lp_vec2f *scale, lp_vec2f *translate)
{
	size_t i;

	for (i = 0; i < N; i++) {
		p[i].x = infinity_conv_axis(p[i].x, scale->x, translate->x);
		p[i].y = infinity_conv_axis(p[i].y, scale->y, translate->y);
	}
}

void lp_program_generic_draw_line_primitive(lp_program_generic *program,
		lp_vec2f *coords, size_t coords_count, lp_color4f *color, float width,
		lp_vec2f *scale, lp_vec2f *translate)
{
	LOG_GL_ERROR(glUseProgram(program->id));
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, 0));

	/* common parameters */
	LOG_GL_ERROR(glLineWidth(width));
	LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1, CAST_TO_FLOAT_PTR(color)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.scale, 1, CAST_TO_FLOAT_PTR(scale)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.translate, 1, CAST_TO_FLOAT_PTR(translate)));

	/* coords */
	LOG_GL_ERROR(glEnableVertexAttribArray(program->attribute.position));
	LOG_GL_ERROR(glVertexAttribPointer(program->attribute.position,
		2, GL_FLOAT, false, 0, CAST_TO_FLOAT_PTR(coords)));

	/* draw */
	LOG_GL_ERROR(glDrawArrays(GL_LINES, 0, coords_count));
}

void lp_program_generic_draw_line(lp_program_generic *program,
		lp_line *line, lp_vec2f *scale, lp_vec2f *translate)
{
	lp_vec2f coords[2] = {
		{line->start.x, line->start.y},
		{line->end.x, line->end.y}
	};

	infinity_conv(coords, 2, scale, translate);

	lp_program_generic_draw_line_primitive(program,
		coords, 2, &line->color, line->width, scale, translate);
}

void lp_program_generic_draw_rectangle(lp_program_generic *program,
		lp_rectangle *rect, lp_vec2f *scale, lp_vec2f *translate)
{
	float left = rect->geom.x;
	float bottom = rect->geom.y;
	float right = isinf(rect->geom.w) ? +INFINITY : (rect->geom.x + rect->geom.w);
	float top = isinf(rect->geom.h) ? +INFINITY : (rect->geom.y + rect->geom.h);

	lp_vec2f coords[4] = {
		{left, bottom},
		{right, bottom},
		{right, top},
		{left, top}
	};

	infinity_conv(coords, 4, scale, translate);

	LOG_GL_ERROR(glUseProgram(program->id));
	LOG_GL_ERROR(glBindBuffer(GL_ARRAY_BUFFER, 0));

	LOG_GL_ERROR(glUniform2fv(program->uniform.scale, 1, CAST_TO_FLOAT_PTR(scale)));
	LOG_GL_ERROR(glUniform2fv(program->uniform.translate, 1, CAST_TO_FLOAT_PTR(translate)));

	/* coords */
	LOG_GL_ERROR(glEnableVertexAttribArray(program->attribute.position));
	LOG_GL_ERROR(glVertexAttribPointer(program->attribute.position,
		2, GL_FLOAT, false, 0, CAST_TO_FLOAT_PTR(coords)));

	if (rect->line.show) {
		/* common parameters */
		lp_color4f *color = &rect->line.color;
		LOG_GL_ERROR(glLineWidth(rect->line.width));
		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1, CAST_TO_FLOAT_PTR(color)));
		LOG_GL_ERROR(glDrawArrays(GL_LINE_LOOP, 0, 4));
	}

	if (rect->fill.show) {
		/* common parameters */
		lp_color4f *color = &rect->fill.color;
		LOG_GL_ERROR(glUniform4fv(program->uniform.color, 1, CAST_TO_FLOAT_PTR(color)));
		LOG_GL_ERROR(glDrawArrays(GL_TRIANGLE_FAN, 0, 4));
	}
}
