/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PROGRAM_CIRCLE_H
#define LIBREPLOT_PROGRAM_CIRCLE_H

#include "common.h"
#include "../extra/circle.h"

__BEGIN_DECLS

struct lp_program_circle {
	GLuint id;
	struct { GLint color, div_angle, radius, center, scale, translate; } uniform;
	struct { GLint index; } attribute;
};

typedef struct lp_program_circle lp_program_circle;

LP_PRIV lp_program_circle *lp_program_circle_gen(lp_program_circle *program);
LP_PRIV void lp_program_circle_del(lp_program_circle *program);

LP_PRIV void lp_program_circle_draw_circle(lp_program_circle *program,
		lp_circle *circle, lp_vec2f *scale, lp_vec2f *translate);

__END_DECLS

#endif
