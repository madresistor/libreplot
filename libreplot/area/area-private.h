/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_AREA_PRIVATE_H
#define LIBREPLOT_AREA_PRIVATE_H

#include "../common.h"
#include "area.h"
#include "../extra/obj-manager-private.h"
#include "../program/generic.h"
#include "../program/circle.h"
#include "../program/text.h"

__BEGIN_DECLS

struct lp_area {
	lp_obj_manager obj_manager;

	lp_vec2f dpi;
	lp_surface surface;

	struct {
		hb_buffer_t *hb_buffer;
	} reuse;

	struct {
		lp_program_generic *generic;
		lp_program_circle *circle;
		lp_program_text *text;
	} program;
};

__END_DECLS

#endif
