/*
 * This file is part of libreplot.
 * Copyright (C) 2015, 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_AREA_H
#define LIBREPLOT_AREA_H

#include "../common.h"
#include "../extra/circle.h"
#include "../extra/line.h"
#include "../extra/text.h"

__BEGIN_DECLS

/*
 * An Area is a Generic OpenGL area on which the user can plot something.
 * It has +1 to -1 coordinate (also, OpenGL default) on operation can be performed.
 */

typedef struct lp_area lp_area;

/* area resource management */

/*
 * use of area argument:
 * just for OpenGL ES.
 * This will regenerate all the OpenGL internals stuff
 *   when the OpenGL ES context is distroyed.
 * This has the benifit of preventing any non-opengl resource reallocation.
 *  (prevent del() and then gen() cycle)
 */
LP_API lp_area *lp_area_gen(void);
LP_API void lp_area_ref(lp_area *area);
LP_API void lp_area_del(lp_area *area);

enum lp_area_part {
	/* 4uint */
	LP_AREA_SURFACE = 0, /* x, y, width, height */

	/* 2float */
	LP_AREA_DPI = 10, /* x, y */

	/* float */
	/* LP_AREA_CIRCLE_APPROX_ARC = 20   future */
};

typedef enum lp_area_part lp_area_part;

LP_API void lp_area_4uint(lp_area *area, lp_area_part part,
			unsigned arg0, unsigned arg1, unsigned arg2, unsigned arg3);
LP_API void lp_area_2float(lp_area *area, lp_area_part part,
			float arg0, float arg1);

/* this function is to be called before any call to sub draw's */
LP_API void lp_area_draw_start(lp_area *area);

/* list of sub-draws supported by area */
LP_API void lp_area_draw_line(lp_area *area, lp_line *line);
LP_API void lp_area_draw_text(lp_area *area, lp_text *text);
LP_API void lp_area_draw_circle(lp_area *area, lp_circle *circle);
LP_API void lp_area_draw_circle_arc(lp_area *area, lp_circle_arc *arc);
LP_API void lp_area_draw_circle_segment(lp_area *area, lp_circle_segment *segment);
LP_API void lp_area_draw_circle_sector(lp_area *area, lp_circle_sector *sector);

/* this function is to be called after all sub-draw's have been finished */
LP_API void lp_area_draw_end(lp_area *area);

LP_API void lp_area_renew(lp_area *area, unsigned tag);

__END_DECLS

#endif
