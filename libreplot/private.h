/*
 * This file is part of libreplot.
 * Copyright (C) 2014-2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * libreplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libreplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libreplot.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBREPLOT_PRIVATE_H
#define LIBREPLOT_PRIVATE_H

#include <stdbool.h>

/* libreplot can only run on hardware that have
 *   atleast GLES >= 2.0
 *  or
 *   atleast GL >= 3.0
 *
 * If we are running on Windows, things can only run properly if a
 *  function loading library is there.
 *  we use libepoxy for that.
 */

/* Via this trick, we can compile libreplot
 *  using run time flags or compile time flags for OpenGL */

/* libepoxy does all the runtime stuff! (instead of compile time) */
#if defined(LP_USE_EPOXY)
# include <epoxy/gl.h>
# define LP_TARGET_GLES (!epoxy_is_desktop_gl())
# define LP_TARGET_VERSION (epoxy_gl_version())

/* use GLES2 */
#elif defined(LP_USE_GLES2)
# include <GLES/gl.h>
# include <GLES2/gl2.h>
# define LP_TARGET_GLES true
# define LP_TARGET_VERSION 20

/* use GLES3 */
#elif defined(LP_USE_GLES3)
# include <GLES/gl.h>
# include <GLES3/gl3.h>
# define LP_TARGET_GLES true
# define LP_TARGET_VERSION 30

/* use GL */
#elif defined(LP_USE_GL3) || defined(LP_USE_GL4)
# define GL_GLEXT_PROTOTYPES /* to prevent this */
# include <GL/gl.h>
# include <GL/glext.h>
# define LP_TARGET_GLES false

/* detect version */
# if defined(GL_VERSION_4_5)
#  define LP_TARGET_VERSION 45
# elif defined(GL_VERSION_4_4)
#  define LP_TARGET_VERSION 44
# elif defined(GL_VERSION_4_3)
#  define LP_TARGET_VERSION 43
# elif defined(GL_VERSION_4_2)
#  define LP_TARGET_VERSION 42
# elif defined(GL_VERSION_4_1)
#  define LP_TARGET_VERSION 41
# elif defined(GL_VERSION_4_0)
#  define LP_TARGET_VERSION 40
# elif defined(GL_VERSION_3_3)
#  define LP_TARGET_VERSION 33
# elif defined(GL_VERSION_3_2)
#  define LP_TARGET_VERSION 32
# elif defined(GL_VERSION_3_1)
#  define LP_TARGET_VERSION 31
# elif defined(GL_VERSION_3_0)
#  define LP_TARGET_VERSION 30
# endif

/* fallback. (though useless) */
# if !defined(LP_TARGET_VERSION)
#  if defined(LP_USE_GL4)
#   define LP_TARGET_VERSION 40
#  elif defined(LP_USE_GL3)
#   define LP_TARGET_VERSION 30
#  endif
# endif
#else
# error GL to use configuration failed
#endif

#include "common.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>

__BEGIN_DECLS

#define UNUSED(var) ((void)var)

#if defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
/* Microsoft Windows specific bullshit */
# define WIN_SPECIFIC_BULLSHIT 1
#endif

/* size_t cross platform print support
 * http://stackoverflow.com/questions/1546789
 */
/* POSIX like printf for size_t */
#if defined(WIN_SPECIFIC_BULLSHIT)
# define LP_PRIS_PREFIX "I"
#elif defined(__GNUC__)
# define LP_PRIS_PREFIX "z"
#else
# error "size_t not supported, please specify"
#endif

#define PRIdS LP_PRIS_PREFIX "d"
#define PRIxS LP_PRIS_PREFIX "x"
#define PRIuS LP_PRIS_PREFIX "u"
#define PRIXS LP_PRIS_PREFIX "X"
#define PRIoS LP_PRIS_PREFIX "o"

#define LOG_GL_ERROR(cmd) {									\
		cmd;															\
		LOG_GL_ERROR_TEST(#cmd);								\
	}

#if !defined(NDEBUG)
# define LOG_GL_ERROR_TEST(msg) {									\
		GLint _err = glGetError();										\
		if (_err != GL_NO_ERROR) {										\
			LOG_WARN("glGetError [%i]: %s", _err, msg);			\
		}																		\
	}
#else
# define LOG_GL_ERROR_TEST(msg)
#endif

#define CAST_TO_FLOAT_PTR(ptr) ((float *)(ptr))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define LOG_TAG "libreplot"

/* debug log */
#if !defined(NDEBUG)
# if __ANDROID__
#  include <android/log.h>
#  define LOG_DEBUG(fmt, ...) \
		__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,	\
			 "%s: " fmt, __FUNCTION__, ##__VA_ARGS__)
# else /* __ANDROID__ */
#  include <stdio.h>
#  define LOG_DEBUG(fmt, ...) \
		fprintf(stdout,	\
			LOG_TAG ": debug: %s: " fmt "\n", __FUNCTION__, ##__VA_ARGS__)
# endif /* __ANDROID__ */
#else /* !defined(NDEBUG) */
# define LOG_DEBUG(fmt, ...)
#endif /* !defined(NDEBUG) */

/* warning log */
#if __ANDROID__
# include <android/log.h>
# define LOG_WARN(fmt, ...) \
		__android_log_print(ANDROID_LOG_WARN, LOG_TAG,	\
			"%s: " fmt, __FUNCTION__, ##__VA_ARGS__)
#else /* __ANDROID__ */
# include <stdio.h>
# define LOG_WARN(fmt, ...) \
		fprintf(stderr,		\
			LOG_TAG ": warn: %s: " fmt "\n", __FUNCTION__, ##__VA_ARGS__)
#endif /* __ANDROID__ */

#define LOG_DEBUG_INT(variable) LOG_DEBUG(#variable ": %i", variable)
#define LOG_DEBUG_FLOAT(variable) LOG_DEBUG(#variable ": %f", variable)

#define LOG_ASSERT_RET_ON_FAIL(stmt, ...)		\
	if (!(stmt)) {										\
		LOG_WARN("%s failed", #stmt);				\
		return __VA_ARGS__;							\
	}

#define LOG_WARN_IF_NULL(ptr)				\
	if (ptr == NULL) {						\
		LOG_WARN("`%s` is null", #ptr);	\
	}

#define LOG_WARN_IF_FAIL(stmt)				\
	if (!(stmt)) {									\
		LOG_WARN("`%s` failed", #stmt);		\
	}

#define LOG_ASSERT_RET_ON_NULL(ptr, ...) \
		LOG_ASSERT_RET_ON_FAIL((ptr) != NULL, ##__VA_ARGS__)

#define LOG_ASSERT_RET_ON_FAIL_ARG(stmt, ...) \
		LOG_ASSERT_RET_ON_FAIL(stmt, ##__VA_ARGS__)

#define LOG_ASSERT_RET_ON_NULL_ARG(ptr, ...) \
		LOG_ASSERT_RET_ON_NULL(ptr, ##__VA_ARGS__)

#define LOG_ASSERT_RET_ON_ALLOC_FAIL(ptr, ...) \
		LOG_ASSERT_RET_ON_NULL(ptr, ##__VA_ARGS__)

#define LP_TRANSLATE_NONE (lp_vec2f){0, 0}
#define LP_SCALE_NONE (lp_vec2f){1, 1}

#define LP_COLOR_WHITE (lp_color4f){1, 1, 1, 1}
#define LP_COLOR_BLACK (lp_color4f){0, 0, 0, 1}
#define LP_COLOR_RED (lp_color4f){1, 0, 0, 1}
#define LP_COLOR_GREEN (lp_color4f){0, 1, 0, 1}
#define LP_COLOR_BLUE (lp_color4f){0, 0, 1, 1}

/* log if the part (ie enum) is incorrect */
#define SWITCH_ENUM_INCORRECT(val) \
	LOG_WARN("%s: Incorrect part: %i", __func__, val)

/* free the object "ptr" but do check if it not NULL */
#define DEL_OBJ(ptr, del_fn)				\
	if ((ptr) != NULL) { del_fn(ptr); }

/* replace one string by another. */
#define REPLACE_OBJ(old_obj, new_obj, type) {								\
	/* remove the old reference */												\
	if ((old_obj) != NULL) {														\
		lp_##type##_del(old_obj);													\
		(old_obj) = NULL;																\
	}																						\
	/* store the new reference */													\
	if ((new_obj) != NULL) {														\
		lp_##type##_ref(new_obj);													\
		(old_obj) = (new_obj);														\
	}																						\
}

#define REPLACE_STR(old, new) {													\
	/* remove the old reference */												\
	if ((old) != NULL) {																\
		free(old);																		\
		(old) = NULL;																	\
	}																						\
	/* store the new reference */													\
	if ((new) != NULL) {																\
		(old) = (uint8_t *) strdup((const char *) (new));					\
		LOG_WARN_IF_NULL(old);														\
	}																						\
}

#define REPLACE_FONT(old, new) \
	REPLACE_OBJ(old, new, font)

#define REPLACE_TEXTURE(old, new) \
	REPLACE_OBJ(old, new, texture)

#define OBJ_REF_FUNC(obj_name)									\
void lp_##obj_name##_ref(lp_##obj_name *obj_name)			\
{																			\
	LOG_ASSERT_RET_ON_NULL_ARG(obj_name);						\
	lp_obj_manager_ref(&obj_name->obj_manager);				\
}

#define RENEW_OBJ(ptr, tag, type)			\
	if ((ptr) != NULL) { lp_##type##_renew(ptr, tag); }

#define RENEW_TEXTURE(ptr, tag) RENEW_OBJ(ptr, tag, texture)

#define SWAP_VALUE(val1, val2, type) {		\
	type tmp;										\
	tmp = val1;										\
	val1 = val2;									\
	val2 = tmp;										\
}

#define UTF8_STR(str, ...) ((uint8_t *) (str))
#define CONST_UTF8_STR(str, ...) ((const uint8_t *) (str))

#define LP_MM_TO_PX(mm, dpi) (((mm) / 25.4) * (dpi))
#define LP_PX_TO_MM(px, dpi) (((px) * 25.4) / (dpi))

#define LP_INCH_TO_MM(inch) ((inch) * 25.4)

#define LP_PX_TO_OPENGL(px, surface_size) (((px) * 2) / (surface_size))
#define LP_OPENGL_TO_PX(oc, surface_size) (((oc) * (surface_size)) / 2)

__END_DECLS

#endif
