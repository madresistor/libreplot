#include "private.h"
#include "common.h"

#if defined(WIN_SPECIFIC_BULLSHIT)
# include <epoxy/wgl.h>
#endif

void lp_init(void)
{
	/* set background to white */
	LOG_GL_ERROR(glClearColor(1, 1, 1, 1));

	/* Antialias */
	LOG_GL_ERROR(glEnable(GL_BLEND));
	LOG_GL_ERROR(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	LOG_GL_ERROR(glEnable(GL_LINE_SMOOTH));
	LOG_GL_ERROR(glDepthMask(GL_FALSE));

	/* enable facility to control point size from vertex shader.
	 *  for GLES, this is enabled by default.
	 */
#if defined(LP_USE_EPOXY) || defined(LP_USE_GL3) || defined(LP_USE_GL4)
	if (!LP_TARGET_GLES) {
		LOG_GL_ERROR(glEnable(GL_POINT_SPRITE));
		LOG_GL_ERROR(glEnable(GL_VERTEX_PROGRAM_POINT_SIZE));
	}
#endif

	glEnable(GL_TEXTURE_2D);
}

void lp_epoxy_handle_external_wglMakeCurrent(void)
{
#if defined(LP_USE_EPOXY) && defined(WIN_SPECIFIC_BULLSHIT)
	epoxy_handle_external_wglMakeCurrent();
#endif
}
